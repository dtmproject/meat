# README #

This README documents whatever steps are necessary to get the application
  DTM++.Project/meat
up and running.

### What is this repository for? ###

* meat: Diffusion Equation Solver using the Mixed Finite-Element-Method (MFEM)
* Version 1.0.0

### How do I get set up? ###

* Dependencies
deal.II v8.3.0 at least, installed via candi, cf. https://github.com/koecher/candi

* Configuration
cmake .
make

* Run (single process)
./meat ./input/default.prm

* Run (multiple processes via MPI)
mpirun -np <Number of Processes> ./meat ./input/default.prm


### Who do I talk to? ###

* Principial Author: Dr.-Ing. Dipl.-Ing. Uwe Koecher (koecher@hsu-hamburg.de)

### License ###
Copyright (C) 2012-2015 by Uwe Koecher

This file is part of DTM++/MEAT.

DTM++/MEAT is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

DTM++/MEAT is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
 
You should have received a copy of the GNU Lesser General Public License
along with DTM++/MEAT. If not, see <http://www.gnu.org/licenses/>.
Please see the file
	./LICENSE
for details.
