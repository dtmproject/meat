/**
 * @file SolverAztecOO.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-02, Rebase to DTM++.core library, UK
 * @date 2015-01-23, meat application, UK
 * @date 2014-06-14, UK
 *
 * @brief SolverAztecOO.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

// PROJECT includes
#include <DTM++/core/lss/SolverAztecOO.hh>

// MPI includes

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/logstream.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/solver.h>

// TRILINOS includes

// C++ includes
#include <memory>
#include <iostream>

namespace DTM {
namespace core {
namespace lss {


SolverAztecOO::
SolverAztecOO(
	std::shared_ptr<dealii::SolverControl> sc,
	std::shared_ptr<Teuchos::ParameterList> solver_parameters) :
	solver_control(sc) {
	// push "SolverAztecOO" prefix to the deallog stream
	dealii::deallog.push("SolverAztecOO");
	
	// check if dealii::SolverControl was given, if not, create a default one
	if (!solver_control.use_count()) {
		solver_control = std::make_shared<dealii::SolverControl> ();
		dealii::deallog
			<< "created dealii::SolverControl object"
			<< ", max_steps = " << solver_control->max_steps()
			<< ", tolerance = " << solver_control->tolerance()
			<< std::endl;
	}
	
	// create the solver instance
	solver = std::make_shared<AztecOO>();
	
	// set solver parameters from Teuchos::ParameterList, if present,
	// otherwise the AztecOO solver uses the default values (GMRES, AZ_kspace=30)
	if (solver_parameters.use_count()) {
		solver->SetParameters(*solver_parameters);
	}
	
	// output the AztecOO solver settings to deallog
	dealii::deallog << "AZ_solver type: ";
	switch (solver->GetAztecOption(AZ_solver)) {
		case AZ_cg:
			dealii::deallog << "AZ_cg";
			break;
		
		case AZ_cg_condnum:
			dealii::deallog << "AZ_cg_condnum";
			break;
		
		case AZ_cgs:
			dealii::deallog << "AZ_cgs";
			break;
		
		case AZ_bicgstab:
			dealii::deallog << "AZ_bicgstab";
			break;
		
		case AZ_gmres:
			dealii::deallog << "AZ_gmres" << std::endl << "AZ_kspace = ";
			dealii::deallog << solver->GetAztecOption(AZ_kspace);
			break;
		
		case AZ_gmres_condnum:
			dealii::deallog << "AZ_gmres_condnum" << std::endl << "AZ_kspace = ";
			dealii::deallog << solver->GetAztecOption(AZ_kspace);
			break;
		
		case AZ_tfqmr:
			dealii::deallog << "AZ_tfqmr";
			break;
			
		default:
			dealii::deallog << "unknown (" << solver->GetAztecOption(AZ_solver) << ")";
	}
	dealii::deallog << std::endl;
	
	dealii::deallog << "AZ_conv type: ";
	switch (solver->GetAztecOption(AZ_conv)) {
		case AZ_noscaled:
			dealii::deallog << "AZ_noscaled";
			break;
		
		default:
			dealii::deallog << "unknown (" << solver->GetAztecOption(AZ_conv) << ")";
	}
	dealii::deallog << std::endl;
	
	dealii::deallog.pop();
}


SolverAztecOO::
~SolverAztecOO() {
}


void
SolverAztecOO::
set_preconditioner(std::shared_ptr<Epetra_Operator> _preconditioner) {
	preconditioner = _preconditioner;
}

void
SolverAztecOO::
solve(
	std::shared_ptr<const dealii::TrilinosWrappers::SparseMatrix> A,
	std::shared_ptr<dealii::TrilinosWrappers::MPI::Vector> x,
	std::shared_ptr<const dealii::TrilinosWrappers::MPI::Vector> b) {
	int ierr;
	
	// Create and set linear problem.
	linear_problem = std::make_shared<Epetra_LinearProblem> (
			const_cast<Epetra_CrsMatrix *>( &(A->trilinos_matrix()) ),
			&(x->trilinos_vector()),
			const_cast<Epetra_MultiVector *>( &(b->trilinos_vector()) )
	);
	
	solver->SetProblem(*linear_problem);
	
	// Set preconditioner
	if (preconditioner.use_count()) {
		ierr = solver->SetPrecOperator(
			const_cast<Epetra_Operator *> (preconditioner.get())
		);
		
		AssertThrow(!ierr, dealii::ExcTrilinosError(ierr));
	}
	else {
		solver->SetAztecOption(AZ_precond, AZ_none);
	}
	
	// solve linear problem
	ierr = solver->Iterate(solver_control->max_steps(), solver_control->tolerance());
	if (ierr) throw int(ierr);
	
	// check for convergence
	solver_control->check(solver->NumIters(), solver->TrueResidual());
	if (solver_control->last_check() != dealii::SolverControl::success) {
		AssertThrow(
			false,
			dealii::SolverControl::NoConvergence(
				solver_control->last_step(), solver_control->last_value()
			)
		);
	}
}


}}} // namespaces
