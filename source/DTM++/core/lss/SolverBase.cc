/**
 * @file SolverBase.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-02, Rebase to DTM++.core library, UK
 * @date 2015-01-23, meat application, UK
 * @date 2014-06-14, UK
 *
 * @brief SolverBase.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

// PROJECT includes
#include <DTM++/core/lss/SolverBase.hh>

// MPI includes

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/solver_control.h>

// TRILINOS includes

// C++ includes
#include <memory>

namespace DTM {
namespace core {
namespace lss {


SolverBase::
SolverBase(
	std::shared_ptr<dealii::SolverControl> sc) :
	solver_control(sc) {
}


SolverBase::
~SolverBase() {
}


void
SolverBase::
solve(
	const DTM::core::lac::OperatorBase &,
	DTM::core::lac::VectorBase &,
	const DTM::core::lac::VectorBase &) {
	// Base class implementation must fail if used.
	AssertThrow(false, dealii::ExcNotImplemented());
}


}}} // namespaces
