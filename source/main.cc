/**
 * @file main.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-08, UK
 * @date 2015-01-23, UK
 * @date 2015-01-13, merge from MixedFE example 2014-01-30_DAAD for ACOMEN PAPER, UK
 * @date 2014-06-16 to 2014-06-20, MEAT, UK
 * @date 2014-09-15, MEAT, UK
 * @date 2013-10-29, origin (Mixed FEM - Heat), UK
 *
 * @brief DTM++/meat: Mixed Finite Element Solver for the Diffusion Equation.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

// DEFINES

////////////////////////////////////////////////////////////////////////////////
// MPI Usage: Limit the numbers of threads to 1, since MPI+X has a poor
// performance.
// Undefine the variable USE_MPI_WITHOUT_THREADS if you want to use MPI+X,
// this would use as many threads per process as deemed useful by TBB.
#define USE_MPI_WITHOUT_THREADS

#ifdef USE_MPI_WITHOUT_THREADS
#define MPIX_THREADS 1
#else
#define MPIX_THREADS dealii::numbers::invalid_unsigned_int
#endif
////////////////////////////////////////////////////////////////////////////////

// PROJECT includes
#include <meat/Parameters/ParameterHandler.tpl.hh>
#include <meat/MEAT_cG1.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/utilities.h>

#include <deal.II/base/mpi.h>
#include <deal.II/base/multithread_info.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <memory>


int main(int argc, char *argv[]) {
	// Init MPI (or MPI+X)
	dealii::Utilities::MPI::MPI_InitFinalize mpi(argc, argv, MPIX_THREADS);
	
	// EVALUATE wall time now.
	auto wall_time_start = MPI_Wtime();
	
	// Get MPI Variables
	const unsigned int MyPID(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD));
	const unsigned int NumProc(dealii::Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD));
	
	// Create individual output file for each running process
	std::ostringstream filename;
	filename << "proc" << MyPID << ".log";
	std::ofstream pout(filename.str().c_str());
	
	try {
		////////////////////////////////////////////////////////////////////////
		// Init application
		//
		
		// Attach deallog to process output
		dealii::deallog.attach(pout);
		dealii::deallog.depth_console(0);
		pout	<< "****************************************"
				<< "****************************************"
				<< std::endl;
		
		pout	<< "Hej, here is process " << MyPID+1 << " from " << NumProc
				<< std::endl;
		
		// Test if an Input Parameter File is potentially given.
		AssertThrow(
			!(argc < 2),
			dealii::ExcMessage (
				std::string ("===>\tUSAGE: ./meat <Input_Parameter_File.prm>"))
		);
		
		// Test if the given Input Parameter File can be opened.
		const std::string input_parameter_filename(argv[1]);
		{
			std::ifstream input_parameter_file(input_parameter_filename.c_str());
			// Test if an Input Parameter File is potentially given.
			AssertThrow(
				input_parameter_file,
				dealii::ExcMessage (
					std::string ("===>\tERROR: Input parameter file <")
					+ input_parameter_filename + "> not found."
				)
			);
		}
		
		// Prepare input parameter handling:
		auto parameter_handler =
			std::make_shared< meat::Parameters::ParameterHandler > ();
		parameter_handler->read_input(argv[1]);
		
		//
		////////////////////////////////////////////////////////////////////////
		
		////////////////////////////////////////////////////////////////////////
		// Begin application
		//
		
		// Setup, initialize and run problem
		switch (parameter_handler->get_integer("dim")) {
			case 2: {
				auto problem = std::make_shared< meat::MEAT_cG1<2> >(pout);
				problem->set_input_parameters(parameter_handler);
				problem->run();
				break;
			}
			
			case 3: {
				auto problem = std::make_shared< meat::MEAT_cG1<3> >(pout);
				problem->set_input_parameters(parameter_handler);
				problem->run();
				break;
			}
			
			default:
				AssertThrow(false, dealii::ExcNotImplemented());
		}
		
		pout << std::endl << "Goodbye." << std::endl;
		
		//
		// End application
		////////////////////////////////////////////////////////////////////////////<
	}
	catch (std::exception &exc) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		pout << std::endl << "Elapsed wall time: " << wall_time_end-wall_time_start << std::endl;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr	<< std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl
						<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
						<< std::endl;
			
			std::cerr	<< exc.what() << std::endl;
			
			std::cerr	<< std::endl
						<< "APPLICATION TERMINATED unexpectedly due to an exception."
						<< std::endl << std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl;
		}
		
		// LOG error message to individual process output file.
		pout	<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
				<< std::endl;
		
		pout	<< exc.what() << std::endl;
		
		pout	<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl;
		
		// Close output file stream
		pout.close();
		
		return 1;
	}
	catch (...) {
		// EVALUATE program run time in terms of the consumed wall time.
		auto wall_time_end = MPI_Wtime();
		pout << std::endl << "Elapsed wall time: " << wall_time_end-wall_time_start << std::endl;
		
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr	<< std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl
						<< "An UNKNOWN EXCEPTION occured!"
						<< std::endl;
			
			std::cerr	<< std::endl
						<< "----------------------------------------"
						<< "----------------------------------------"
						<< std::endl << std::endl
						<< "Further information:" << std::endl
						<< "\tThe main() function catched an exception"
						<< std::endl
						<< "\twhich is not inherited from std::exception."
						<< std::endl
						<< "\tYou have probably called 'throw' somewhere,"
						<< std::endl
						<< "\tif you do not have done this, please contact the authors!"
						<< std::endl << std::endl
						<< "----------------------------------------"
						<< "----------------------------------------"
						<< std::endl;
			
			std::cerr	<< std::endl
						<< "APPLICATION TERMINATED unexpectedly due to an exception."
						<< std::endl << std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl;
		}
		
		// LOG error message to individual process output file.
		pout	<< std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl
				<< "An UNKNOWN EXCEPTION occured!"
				<< std::endl;
		
		pout	<< std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl << std::endl
				<< "Further information:" << std::endl
				<< "\tThe main() function catched an exception"
				<< std::endl
				<< "\twhich is not inherited from std::exception."
				<< std::endl
				<< "\tYou have probably called 'throw' somewhere,"
				<< std::endl
				<< "\tif you do not have done this, please contact the authors!"
				<< std::endl << std::endl
				<< "----------------------------------------"
				<< "----------------------------------------"
				<< std::endl;
		
		pout	<< std::endl
				<< "APPLICATION TERMINATED unexpectedly due to an exception."
				<< std::endl << std::endl
				<< "****************************************"
				<< "****************************************"
				<< std::endl << std::endl;
		
		// Close output file stream
		pout.close();
		
		return 1;
	}

	// EVALUATE program run time in terms of the consumed wall time.
	auto wall_time_end = MPI_Wtime();
	pout << std::endl << "Elapsed wall time: " << wall_time_end-wall_time_start << std::endl;
	
	// Close output file stream
	pout.close();
	
	return 0;
}
