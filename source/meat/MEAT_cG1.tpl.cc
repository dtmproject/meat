/**
 * @file MEAT_cG1.tpl.cc
 * @author Uwe Koecher
 * @date 2015-09-07 (cmake), UK
 * @date 2014-09-14 (updated to MEAT confirmity), UK
 * @date 2013-10-17 (initial release derived from stationary Laplace problem)
 *
 * @brief MEAT cG1 solver
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

// PROJECT includes
#include <meat/MEAT_cG1.tpl.hh>

// DTM++ includes

// C++ includes

namespace meat {

/// Constructor.
template<int dim>
MEAT_cG1<dim>::
MEAT_cG1(
	std::ofstream &pout) :
	pout(pout),
	pcout(std::cout, !dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
}


template<int dim>
void
MEAT_cG1<dim>::
set_input_parameters(
	std::shared_ptr< dealii::ParameterHandler > parameter_handler) {
	parameter_set = std::make_shared< meat::Parameters::ParameterSet > (
		parameter_handler
	);
}


/// Set inverse permeability TensorFunction.
template<int dim>
void
MEAT_cG1<dim>::
set_inverse_permeability_tensor(
	std::shared_ptr< dealii::TensorFunction<2,dim> > fun) {
	inverse_permeability = fun;
}


/// Set Dirichlet boundary function.
template<int dim>
void
MEAT_cG1<dim>::
set_dirichlet_boundary(
	std::shared_ptr< dealii::Function<dim> > fun) {
	dirichlet_boundary = fun;
}


/// Set force function.
template<int dim>
void
MEAT_cG1<dim>::
set_force(
	std::shared_ptr< dealii::Function<dim> > fun) {
	force = fun;
}


/// Set initial value function.
template<int dim>
void
MEAT_cG1<dim>::
set_u0(
	std::shared_ptr< dealii::Function<dim> > fun) {
	initial_value_function_u0 = fun;
}


/// Set initial time discretisation parameter tau.
template<int dim>
void
MEAT_cG1<dim>::
set_initial_tau(const double _tau) {
	tau = _tau;
}


template<int dim>
void
MEAT_cG1<dim>::
set_data_output_trigger(double _data_output_trigger) {
	data_output_trigger = _data_output_trigger;
}


template<int dim>
void
MEAT_cG1<dim>::
set_data_output_patches(unsigned int _data_output_patches) {
	data_output_patches = _data_output_patches;
}


template<int dim>
void
MEAT_cG1<dim>::
create_function(
	std::string &function_string,
	std::shared_ptr< dealii::Function<dim> > &function) {
	if (function_string.compare("ZeroFunction") == 0) {
		function = std::make_shared< dealii::ZeroFunction<dim> > (1);
	}
}

template<int dim>
void
MEAT_cG1<dim>::
init_functions_from_parameters() {
	// TODO
	if (parameter_set->inverse_permeability_tensor.compare("Inverse_anyD_Identity") == 0) {
		inverse_permeability =
			std::make_shared< meat::PermeabilityTensors::Inverse_anyD_Identity<2,dim> > ();
	}
	else if (parameter_set->inverse_permeability_tensor.compare("Inverse_3D_ACOMEN") == 0) {
		inverse_permeability =
			std::make_shared< meat::PermeabilityTensors::Inverse_3D_ACOMEN<2,dim> > ();
	}
	else {
		AssertThrow(false, dealii::ExcNotImplemented());
	}
	
	if (parameter_set->dirichlet_boundary.compare("dirichlet_3D_ACOMEN") == 0) {
		dirichlet_boundary =
			std::make_shared< meat::BoundaryValues::dirichlet_ACOMEN<dim> >(
				parameter_set->dirichlet_boundary_parameters
			);
	}
	else {
		create_function(parameter_set->dirichlet_boundary, dirichlet_boundary);
	}
	
	create_function(parameter_set->force, force);
	Assert(force.use_count(), dealii::ExcInternalError());
	
	create_function(parameter_set->u0, initial_value_function_u0);
	Assert(initial_value_function_u0.use_count(), dealii::ExcInternalError());
}


/// Initialise MEAT_cG1's problem.
template<int dim>
void
MEAT_cG1<dim>::
init() {
	Assert(parameter_set.use_count(), dealii::ExcNotInitialized());
	init_functions_from_parameters();
	
	p = parameter_set->p;
	const unsigned int global_refinement = parameter_set->r;
	
	// create finite element system
	fe = std::make_shared< dealii::FESystem<dim> > (
		dealii::FE_RaviartThomas<dim> (p), 1, // flux FE
		dealii::FE_DGQArbitraryNodes<dim> (dealii::QGauss<1>(p+1)), 1 // solution FE
	);
	
	// create desired mapping (boundary mapping)
	mapping = nullptr;
	
	// create grid instance
	grid = std::make_shared< meat::Grid::Grid<dim> > (
		fe, mapping
	);
	
	// TODO: not fine implemented, but it works so far
	if (parameter_set->use_mesh_input_file) {
		AssertThrow(false, dealii::ExcNotImplemented());
	}
	else {
		std::string GridGenerator = "hyper_cube";
		if (GridGenerator.compare(parameter_set->GridGenerator) == 0) {
			// Generate hyper cubic grid
			Assert(grid.use_count(), dealii::ExcNotInitialized());
			dealii::GridGenerator::hyper_cube(
				*(grid->tria),
				parameter_set->a,
				parameter_set->b
			);
		}
		else {
			AssertThrow(false, dealii::ExcNotImplemented());
		}
	}
	
	// Refine the mesh globally through dividing each cell n times.
	grid->refine_global(global_refinement);
	
	pout << "Number of locally owned active cells: " <<
		grid->tria->n_locally_owned_active_cells() << std::endl;
	pout << "Number of globally active cells:      " <<
		grid->tria->n_global_active_cells() << std::endl;
	
	// Distribute degrees of freedom
	grid->distribute();
	
	pout << "Finite Element Space: {RT,FE_DGQ} with degree p = "
		<< p << std::endl;
	pout << "DoFs:                                 " <<
		grid->dof->n_dofs() << std::endl;
	
	pout << "DoFs N_u:                             " <<
		grid->n_u << std::endl;
	pout << "DoFs N_q:                             " <<
		grid->n_q << std::endl;
	
	// Colorise the boundaries:
	//  * Dirichlet boundary    : meat::types::boundary_id::Dirichlet
	// 
	// Looping over all cells, than over all faces of a cell, because in deal.II
	// is no direct mapping to faces available, only from cell to its faces.
	Assert(grid->tria.use_count(), dealii::ExcNotInitialized());
	auto cell(grid->tria->begin_active());
	auto end(grid->tria->end());
	
	for( ; cell != end; ++cell)
	for (unsigned int face(0); face < dealii::GeometryInfo<dim>::faces_per_cell; ++face) {
		if ( cell->face(face)->at_boundary() ) {
			cell->face(face)->set_boundary_id(
				static_cast<dealii::types::boundary_id> (
					meat::types::boundary_id::Dirichlet)
			);
		}
	}
	
	// initialize vectors
	Assert(grid->partitioning_locally_owned_dofs.use_count(), dealii::ExcNotInitialized());
	x0.reinit(*(grid->partitioning_locally_owned_dofs), grid->mpi_comm);
	x1.reinit(*(grid->partitioning_locally_owned_dofs), grid->mpi_comm);
	xn.reinit(*(grid->partitioning_locally_owned_dofs), grid->mpi_comm);
	
	x_trigger = std::make_shared< dealii::TrilinosWrappers::MPI::BlockVector > ();
	x_trigger->reinit(*(grid->partitioning_locally_owned_dofs), grid->mpi_comm);
	
	bb1.reinit(*(grid->partitioning_locally_owned_dofs), grid->mpi_comm);
	
	vec_M_u0.reinit(x0.block(1));
	
	b.reinit(x0.block(1));
	
	qq.reinit(x0.block(0));
	
	// create rhs assemblers
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	
	std::unique_ptr< meat::Assemble::RHS::Assembler<dim> > asm1(
		new meat::Assemble::RHS::Assembler<dim> (
			bb1,
			*(grid->constraints),
			*(grid->fe),
			*(grid->mapping),
			*(grid->dof)
		)
	);
	assembler_rhs_1 = std::move(asm1);
	
	// INIT DATA OUTPUT ////////////////////////////////////////////////////////
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	Assert(grid->partitioning_locally_owned_dofs.use_count(), dealii::ExcNotInitialized());
	Assert(grid->partitioning_locally_relevant_dofs.use_count(), dealii::ExcNotInitialized());
	
	// h5/vtu/vtk-data output trigger
	set_data_output_trigger(parameter_set->data_output_trigger);
	// h5/vtu/vtk-data output patches
	set_data_output_patches(parameter_set->data_output_patches);
	
	std::vector<std::string> data_field_names;
	for (unsigned int i(0); i < dim; ++i)
		data_field_names.push_back("flux");
	data_field_names.push_back("temperature");
	
	std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation > dci_field;
	for (unsigned int i(0); i < dim; ++i)
		dci_field.push_back(dealii::DataComponentInterpretation::component_is_part_of_vector);
	dci_field.push_back(dealii::DataComponentInterpretation::component_is_scalar);
	
	data_output.set_DoF_data(
		grid->dof,
		grid->partitioning_locally_owned_dofs,
		grid->partitioning_locally_relevant_dofs
	);
	
	data_output.set_data_field_names(data_field_names);
	data_output.set_data_component_interpretation_field(dci_field);
	data_output.set_data_output_patches(data_output_patches);
}


/// Run MEAT_cG1's problem.
template<int dim>
void
MEAT_cG1<dim>::
run() {
	init();
	
	Assert(parameter_set.use_count(), dealii::ExcNotInitialized());
	const double _t0 = parameter_set->t0;
	const double tend = parameter_set->T;
	set_initial_tau(parameter_set->tau_n);
	
	// prepare temporal iteration //////////////////////////////////////////////
	Assert(tau != 0, dealii::ExcInvalidState());
	double t0 = _t0;
	double t1 = t0 + .5*tau;
	double tn = t0 + tau;
	
	data_output_time_value = _t0;
	
	// setup initial system ////////////////////////////////////////////////////
	// assemble L = [M_q B, B^T 0], and M, iM
	assemble_system();
	// iMq = M_q^{-1}; L = [M_q, B; B^T 0]
	iMq = std::make_shared< meat::lac::MEAT_Matrix_iMq
		< dealii::TrilinosWrappers::MPI::Vector > >(
		L
	);
	
	// A = B^T M_q^{-1} B
	auto A = std::make_shared< meat::lac::MEAT_Matrix_A
		< dealii::TrilinosWrappers::MPI::Vector > >(
		L, iMq,
		x0.block(0)
	);
	
	// ~A = B^T diag(M_q)^{-1} B
	auto aA = std::make_shared< meat::lac::MEAT_Matrix_aA
		< dealii::TrilinosWrappers::MPI::Vector > >(
		L,
		x0.block(0)
	);
	
	// A_1 = M + tau/2. A
	A1 = std::make_shared< meat::lac::MEAT_Matrix_K
		< dealii::TrilinosWrappers::MPI::Vector > >(
		M, A,
		1.,
		tau,
		x0.block(1)
	);
	
	// Preconditioner for solving y = A_1^{-1} x.
	// Applies: preconditioner_aA1 * x = (M + tau/2 ~A) * x,
	// with ~A = B^T diag(M_q)^{-1} B; cf. see above.
	auto aA1 = std::make_shared< meat::lac::MEAT_Matrix_K
		< dealii::TrilinosWrappers::MPI::Vector > >(
		M, aA,
		1.,
		tau,
		x0.block(1)
	);
	
	auto preconditioner_aA1 = std::make_shared< meat::lac::MEAT_Matrix_Inverse
		< dealii::TrilinosWrappers::MPI::Vector > >(
		aA1
	);
	
	// write initial mesh
	data_output.write_mesh("mesh", _t0);
	
	////////////////////////////////////////////////////////////////////////////
	// shortcut references
	dealii::TrilinosWrappers::MPI::Vector &q0(x0.block(0));
	dealii::TrilinosWrappers::MPI::Vector &q1(x1.block(0));
// 	dealii::TrilinosWrappers::MPI::Vector &qn(xn.block(0));
	
	dealii::TrilinosWrappers::MPI::Vector &u0(x0.block(1));
	dealii::TrilinosWrappers::MPI::Vector &u1(x1.block(1));
// 	dealii::TrilinosWrappers::MPI::Vector &un(xn.block(1));
	
	dealii::TrilinosWrappers::MPI::Vector &g1(bb1.block(0));
	
	dealii::TrilinosWrappers::SparseMatrix &B(L->block(0,1));
	
	////////////////////////////////////////////////////////////////////////////
	// setup initial values ////////////////////////////////////////////////////
	// interpolate/project initial pressure
	std::unique_ptr<dealii::TrilinosWrappers::MPI::BlockVector> block_vec_g0_u0(
		new dealii::TrilinosWrappers::MPI::BlockVector()
	);
	block_vec_g0_u0->reinit(*(grid->partitioning_locally_owned_dofs), grid->mpi_comm);
	
	auto assembler_u0 = std::make_shared< meat::Assemble::RHS::Assembler<dim> >(
		*block_vec_g0_u0,
		*(grid->constraints),
		*(grid->fe),
		*(grid->mapping),
		*(grid->dof)
	);
	
	initial_value_function_u0->set_time(t0);
	dirichlet_boundary->set_time(t0);
	assembler_u0->assemble(
		initial_value_function_u0, dirichlet_boundary, true
	);
	assembler_u0.reset();
	
	// Coefficients for u0 are calculated as: u0 = M^{-1} * u_0(t0)
	iM->vmult(u0, block_vec_g0_u0->block(1));
	
	// Coefficients for q0 are calculated with Darcys law in t0 as
	// q0 = M^{-1} ( - g0 - B*u0 )
	B.vmult(qq, u0);
	qq.sadd(-1., -1., block_vec_g0_u0->block(0));
	iMq->vmult(q0, qq);
	
	block_vec_g0_u0.reset();
	////////////////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////////////////////
	// prepare LSS, solve for temperature u1
	auto solver_control = std::make_shared<dealii::SolverControl>(
		parameter_set->outer_solver_max_iter,
		parameter_set->outer_solver_tolerance
	);
	
	dealii::SolverCG<dealii::TrilinosWrappers::MPI::Vector>::
	AdditionalData additional_solver_data;
	additional_solver_data.compute_all_condition_numbers =
		parameter_set->compute_all_condition_numbers;
	
	auto solver = std::make_shared<dealii::SolverCG
		<dealii::TrilinosWrappers::MPI::Vector>>(
		*solver_control,
		additional_solver_data
	);
	////////////////////////////////////////////////////////////////////////////
	
	// temporal iteration /////////////////////////////////////////////////////////
	tn = _t0; // set initial constraint for iteration
	xn = x0;  // set initial constraint for iteration
	for (unsigned int n = 1; n*tau < tend+tau/2.; ++n) {
		t0 = tn;
		t1 = t0 + .5*tau;
		tn = t0 + tau;
		
		// swap conitnuity condition
		x0 = xn;
		
		////////////////////////////////////////////////////////////////////////
		// solve for temperature unknowns
		
		// Assemble right hand sides and
		// calculate outer Schur complement vectors: b1, b2 and b.
		assemble_rhs(t1);
		
		// Solve for u1
		u1 = u0; // set initial guess for solution (u1 ~= u0)
		solver->solve(*A1, u1, b, *preconditioner_aA1);
		
		// Update flux field:
		//   q1 = M_q^{-1} (-g1 -B*u1).
		B.vmult(qq, u1);
		qq.sadd(-1., -1., g1);
		iMq->vmult(q1, qq);
		
		// Calculate by evaluation of time trial function xn = [q(t_n); u(t_n)]
		xn.equ(-1., x0);
		xn.add(2., x1);
		
		// write out the solution
		do_data_output(data_output_time_value, t0, tn, data_output_trigger);
	}
	
	pout << "solving done" << std::endl;
}


/// Assemble system.
template<int dim>
void
MEAT_cG1<dim>::
assemble_system() {
	// INIT ////////////////////////////////////////////////////////////////////
	Assert(grid->sp_block_L.use_count(), dealii::ExcNotInitialized());
	Assert(grid->sp_block_M.use_count(), dealii::ExcNotInitialized());
	L = std::make_shared<dealii::TrilinosWrappers::BlockSparseMatrix>();
	L->reinit(*grid->sp_block_L);
	
	auto M_block = std::make_shared<dealii::TrilinosWrappers::BlockSparseMatrix>();
	M_block->reinit(*grid->sp_block_M);
	
	auto iM_block = std::make_shared<dealii::TrilinosWrappers::BlockSparseMatrix>();
	iM_block->reinit(*grid->sp_block_M);
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	*L = 0;
	*M_block = 0;
	*iM_block = 0;
	
	Assert(grid->constraints.use_count(), dealii::ExcNotInitialized());
	Assert(grid->fe.use_count(), dealii::ExcNotInitialized());
	Assert(grid->dof.use_count(), dealii::ExcNotInitialized());
	meat::Assemble::Assembler<dim> assemble_cell_terms(
		L, M_block, iM_block,
		*(grid->constraints), *(grid->fe), *(grid->mapping), *(grid->dof)
	);
	assemble_cell_terms.assemble(inverse_permeability, true);
	
	// copy mass and inverse mass matricies into TrilinosWrappers::SparseMatrix
	M = std::make_shared< dealii::TrilinosWrappers::SparseMatrix >();
	M->reinit(M_block->block(1,1));
	M->copy_from(M_block->block(1,1));
	
	iM = std::make_shared< dealii::TrilinosWrappers::SparseMatrix >();
	iM->reinit(iM_block->block(1,1));
	iM->copy_from(iM_block->block(1,1));
}

/// Assemble system.
template<int dim>
void
MEAT_cG1<dim>::
assemble_rhs(const double t1) {
	// ASSEMBLY ////////////////////////////////////////////////////////////////
	bb1 = 0;
	dirichlet_boundary->set_time(t1);
	force->set_time(t1);
	assembler_rhs_1->assemble(force, dirichlet_boundary, true);
	
	////////////////////////////////////////////////////////////////////////////
	// Build Schur complement right hand side vectors b1, b2, b ////////////////
	
	// shortcut reference access
	dealii::TrilinosWrappers::MPI::Vector &u0(x0.block(1));
	
	dealii::TrilinosWrappers::MPI::Vector &g1(bb1.block(0));
	dealii::TrilinosWrappers::MPI::Vector &f1(bb1.block(1));
	
	dealii::TrilinosWrappers::SparseMatrix &B_transpose(L->block(1,0));
	
	M->vmult(vec_M_u0, u0);
	
	// b = (- tau/2 B^T M_q^{-1} g1) + tau/2 * f1  - a10 u0
	iMq->vmult(qq,g1);
	B_transpose.vmult(b,qq);
	b.sadd(-tau/2., tau/2., f1);
	b.add(1., vec_M_u0);
}


template<int dim>
void
MEAT_cG1<dim>::
do_data_output(double &t, const double t0, const double tn, const double trigger) {
	if (trigger <= 0) return;
	
	// trial functions on \hat I = [0,1]
	double xi0, xi1;
	// time on \hat I = [0,1]
	double _t;
	
	// trial function nodes on \hat I = [0,1]
	const double _t0 = 0;
	const double _t1 = 1./2.;
	
	// write data at triggered time
	for ( ; t < tn+(trigger/2.); t += trigger) {
		_t = (t - t0)/tau;
		
		xi0 = (_t - _t1)/(_t0 - _t1);
		xi1 = (_t - _t0)/(_t1 - _t0);
		
		x_trigger->equ(xi0, x0);
		x_trigger->add(xi1, x1);
		
		data_output.write_data("solution", x_trigger, t);
	}
}

} // namespaces

#include "MEAT_cG1.inst.in"
