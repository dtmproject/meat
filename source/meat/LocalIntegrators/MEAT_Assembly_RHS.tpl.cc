/**
 * @file MEAT_Assembly_RHS.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-06, cmake, UK
 * @date 2014-06-16, MEAT, UK
 * @date 2013-10-17, MFEM, UK
 *
 * @brief Purpose: Assemble right hand side of mixed FEM Heat problem.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

// PROJECT includes
#include <meat/LocalIntegrators/MEAT_Assembly_RHS.tpl.hh>

// DEAL.II includes

// C++ includes

namespace meat {
namespace Assemble {
namespace RHS {

namespace Assembly {
namespace Scratch {

/// (Struct-) Constructor.
template <int dim>
MeatAssemblyRHS<dim>::MeatAssemblyRHS(
	const dealii::FiniteElement<dim> &fe,
	const dealii::Mapping<dim> &,
	const dealii::Quadrature<dim> &quad,
	const dealii::Quadrature<dim-1> &quad_face,
	const dealii::UpdateFlags &uflags_cell,
	const dealii::UpdateFlags &uflags_face) :
	fe_values(fe, quad, uflags_cell),
	fe_face_values(fe, quad_face, uflags_face),
	phi(fe.dofs_per_cell),
	psi(fe.dofs_per_cell),
	f_values(quad.size(),0.),
	g_values(quad_face.size(),0.),
	normal_vector(quad.size()),
	JxW(0),
	dofs_per_cell(0) {
}


/// (Struct-) Copy constructor.
template<int dim>
MeatAssemblyRHS<dim>::MeatAssemblyRHS(const MeatAssemblyRHS &scratch) :
	fe_values(
		scratch.fe_values.get_fe(),
		scratch.fe_values.get_quadrature(),
		scratch.fe_values.get_update_flags()),
	fe_face_values(
		scratch.fe_face_values.get_fe(),
		scratch.fe_face_values.get_quadrature(),
		scratch.fe_face_values.get_update_flags()),
	phi(scratch.phi),
	psi(scratch.psi),
	f_values(scratch.f_values),
	g_values(scratch.g_values),
	normal_vector(scratch.normal_vector),
	JxW(scratch.JxW),
	dofs_per_cell(scratch.dofs_per_cell) {
}

} // namespace Scratch
namespace CopyData {

/// (Struct-) Constructor.
template<int dim>
MeatAssemblyRHS<dim>::MeatAssemblyRHS(const dealii::FiniteElement<dim> &fe) :
	bi_vi_vector(fe.dofs_per_cell),
	local_dof_indices(fe.dofs_per_cell) {
}


/// (Struct-) Copy constructor.
template<int dim>
MeatAssemblyRHS<dim>::MeatAssemblyRHS(const MeatAssemblyRHS &copydata) :
	bi_vi_vector(copydata.bi_vi_vector),
	local_dof_indices(copydata.local_dof_indices) {
}

} // namespace CopyData
} // namespace Assembly
////////////////////////////////////////////////////////////////////////////////


/// Constructor.
template<int dim>
Assembler<dim>::Assembler(
	dealii::TrilinosWrappers::MPI::BlockVector &b,
	dealii::ConstraintMatrix &constraints,
	dealii::FiniteElement<dim> &fe,
	dealii::Mapping<dim> &mapping,
	dealii::DoFHandler<dim> &dof,
	dealii::UpdateFlags uflags_cell,
	dealii::UpdateFlags uflags_face) :
	b(b),
	constraints(constraints),
	fe(fe),
	mapping(mapping),
	dof(dof),
	uflags_cell(uflags_cell),
	uflags_face(uflags_face),
	flux(0), pressure(dim) {
	AssertThrow(dim==2 || dim==3, dealii::ExcNotImplemented());
}


template<int dim>
void Assembler<dim>::assemble(
	std::shared_ptr< dealii::Function<dim> > f,
	std::shared_ptr< dealii::Function<dim> > g,
	const bool compress,
	const unsigned int q) {
	force = f;
	dirichlet_boundary = g;

	// assemble matrix
	const dealii::QGauss<dim> quad(q ? q : (fe.tensor_degree()+1));
	const dealii::QGauss<dim-1> quad_face(q ? q : (fe.tensor_degree()+1));
	
	typedef dealii::FilteredIterator<const typename dealii::DoFHandler<dim>::active_cell_iterator>
		CellFilter;

	// Using WorkStream to assemble.
	dealii::WorkStream::
	run(
		CellFilter(dealii::IteratorFilters::LocallyOwnedCell(), dof.begin_active()),
		CellFilter(dealii::IteratorFilters::LocallyOwnedCell(), dof.end()),
		std::bind (&Assembler::local_assemble_cell, this,
			std::placeholders::_1, std::placeholders::_2, std::placeholders::_3),
		std::bind (&Assembler::copy_local_to_global_cell, this,
			std::placeholders::_1),
		Assembly::Scratch::MeatAssemblyRHS<dim> (
			fe, mapping, quad, quad_face, uflags_cell, uflags_face),
		Assembly::CopyData::MeatAssemblyRHS<dim> (fe)
	);
	
	// Send all data (or even those from artificial cells) to all procs.
	if (compress) b.compress(dealii::VectorOperation::add);
}


/// Local assemble on cell.
template<int dim>
void Assembler<dim>::local_assemble_cell(
	const typename dealii::DoFHandler<dim>::active_cell_iterator &cell,
	Assembly::Scratch::MeatAssemblyRHS<dim> &scratch,
	Assembly::CopyData::MeatAssemblyRHS<dim> &copydata) {
	
	// reinit scratch and data to current cell
	scratch.fe_values.reinit(cell);
	scratch.dofs_per_cell = scratch.fe_values.get_fe().dofs_per_cell;
	cell->get_dof_indices(copydata.local_dof_indices);
	
	// initialize local matrix with zeros
	copydata.bi_vi_vector = 0;
	
	// evaluate f(x_q) & g(x_q) on all quadrature points
	force->value_list(
		scratch.fe_values.get_quadrature_points(),
		scratch.f_values
	);
	
	// assemble cell terms
	for (unsigned int q(0); q < scratch.fe_values.n_quadrature_points; ++q) {
		scratch.JxW = scratch.fe_values.JxW(q);
		
		// prefetch data
		for (unsigned int n(0); n < scratch.dofs_per_cell; n++) {
			scratch.psi[n] =
				scratch.fe_values[pressure].value(n,q);
		}
		
		// loop over all trial & test function combinitions to get the assembly
		for (unsigned int i(0); i < scratch.dofs_per_cell; ++i) {
			copydata.bi_vi_vector(i) +=
				scratch.f_values[q] * scratch.psi[i] * scratch.JxW;
		} // for i
	} // for q
	
	// now loop over all faces
	for (unsigned int face_no(0); face_no < dealii::GeometryInfo<dim>::faces_per_cell;
		 ++face_no) {
		// setup face iterator
		const typename dealii::DoFHandler<dim>::face_iterator &face(cell->face(face_no));
		if (face->at_boundary()) {
		
		// initialize FEValuesFace in scratch object on this cell and face
		scratch.fe_face_values.reinit(cell, face_no);
		
		dirichlet_boundary->value_list(
			scratch.fe_face_values.get_quadrature_points(),
			scratch.g_values
		);
		
		for (unsigned int q(0); q < scratch.fe_face_values.n_quadrature_points; ++q) {
			scratch.normal_vector[q] = scratch.fe_face_values.normal_vector(q);
		}
		
		for (unsigned int q(0); q < scratch.fe_face_values.n_quadrature_points; ++q) {
			scratch.JxW = scratch.fe_face_values.JxW(q);
			
			// prefetch data
			for (unsigned int n(0); n < scratch.dofs_per_cell; n++) {
				scratch.phi[n] =
					scratch.fe_face_values[flux].value(n,q);
			}
			
			// loop over all trial & test function combinitions to get the assembly
			for (unsigned int i(0); i < scratch.dofs_per_cell; ++i) {
				copydata.bi_vi_vector(i) +=
					scratch.g_values[q] *
					scratch.phi[i] * scratch.normal_vector[q] * scratch.JxW;
			} // for i
		} // for q
		} // face->at_boundary
	} // for face_no
}


/// Copy local assembly to global matrix.
template<int dim>
void Assembler<dim>::copy_local_to_global_cell(
	const Assembly::CopyData::MeatAssemblyRHS<dim> &copydata) {
	constraints.distribute_local_to_global(
		copydata.bi_vi_vector, copydata.local_dof_indices, b);
}

}}} // namespaces

#include "MEAT_Assembly_RHS.inst.in"
