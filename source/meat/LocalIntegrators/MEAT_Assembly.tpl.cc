/**
 * @file MEAT_Assembly.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, cmake, UK
 * @date 2014-06-17, MEAT, UK
 * @date 2014-06-16, MEAT, UK
 * @date 2013-10-17, MFEM, UK
 *
 * @brief Purpose: Assemble MFEM_Heat_Assembly
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

// PROJECT includes
#include <meat/LocalIntegrators/MEAT_Assembly.tpl.hh>

// DEAL.II includes


// C++ includes

namespace meat {
namespace Assemble {

namespace Assembly {
namespace Scratch {


/// (Struct-) Constructor.
template <int dim>
MeatAssembly<dim>::MeatAssembly(
	const dealii::FiniteElement<dim> &fe,
	const dealii::Mapping<dim> &,
	const dealii::Quadrature<dim> &quad,
	const dealii::UpdateFlags &uflags_cell) :
	fe_values(fe, quad, uflags_cell),
	phi(fe.dofs_per_cell),
	div_phi(fe.dofs_per_cell),
	psi(fe.dofs_per_cell),
	JxW(0),
	dofs_per_cell(0),
	iD_values(quad.size())  {
}


/// (Struct-) Copy constructor.
template<int dim>
MeatAssembly<dim>::MeatAssembly(const MeatAssembly &scratch) :
	fe_values(
		scratch.fe_values.get_fe(),
		scratch.fe_values.get_quadrature(),
		scratch.fe_values.get_update_flags()),
	phi(scratch.phi),
	div_phi(scratch.div_phi),
	psi(scratch.psi),
	JxW(scratch.JxW),
	dofs_per_cell(scratch.dofs_per_cell),
	iD_values(scratch.iD_values) {
}

} // namespace Scratch
namespace CopyData {

/// (Struct-) Constructor.
template<int dim>
MeatAssembly<dim>::MeatAssembly(const dealii::FiniteElement<dim> &fe) :
	ui_vi_matrix_block_L(fe.dofs_per_cell, fe.dofs_per_cell),
	ui_vi_matrix_block_M(fe.dofs_per_cell, fe.dofs_per_cell),
	ui_vi_matrix_block_iM(fe.dofs_per_cell, fe.dofs_per_cell),
	local_dof_indices(fe.dofs_per_cell) {
}


/// (Struct-) Copy constructor.
template<int dim>
MeatAssembly<dim>::MeatAssembly(const MeatAssembly &copydata) :
	ui_vi_matrix_block_L(copydata.ui_vi_matrix_block_L),
	ui_vi_matrix_block_M(copydata.ui_vi_matrix_block_M),
	ui_vi_matrix_block_iM(copydata.ui_vi_matrix_block_iM),
	local_dof_indices(copydata.local_dof_indices) {
}

} // namespace CopyData
} // namespace Assembly
////////////////////////////////////////////////////////////////////////////////


/// Constructor.
template<int dim>
Assembler<dim>::Assembler(
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L,
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > M,
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > iM,
	dealii::ConstraintMatrix &constraints,
	dealii::FiniteElement<dim> &fe,
	dealii::Mapping<dim> &mapping,
	dealii::DoFHandler<dim> &dof,
	dealii::UpdateFlags uflags) :
	L(L), M(M), iM(iM),
	constraints(constraints),
	fe(fe),
	mapping(mapping),
	dof(dof),
	uflags(uflags),
	flux(0), pressure(dim) {
	Assert(dim==2 || dim==3, dealii::ExcNotImplemented());
}


template<int dim>
void Assembler<dim>::assemble(
	std::shared_ptr< dealii::TensorFunction<2,dim> > _iD,
	const bool compress,
	const unsigned int q) {
	iD = _iD;
	
	// assemble matrix
	const dealii::QGauss<dim> quad(q ? q : (fe.tensor_degree()+1));
	
	typedef dealii::FilteredIterator<const typename dealii::DoFHandler<dim>::active_cell_iterator>
		CellFilter;
	
	// Using WorkStream to assemble.
	dealii::WorkStream::
	run(
		CellFilter(dealii::IteratorFilters::LocallyOwnedCell(), dof.begin_active()),
		CellFilter(dealii::IteratorFilters::LocallyOwnedCell(), dof.end()),
		std::bind (&Assembler::local_assemble_cell, this,
			std::placeholders::_1, std::placeholders::_2, std::placeholders::_3),
		std::bind (&Assembler::copy_local_to_global_cell, this,
			std::placeholders::_1),
		Assembly::Scratch::MeatAssembly<dim> (fe, mapping, quad, uflags),
		Assembly::CopyData::MeatAssembly<dim> (fe)
	);
	
	// Send all data (or even those from artificial cells) to all procs.
	if (compress) {
		L->compress(dealii::VectorOperation::add);
		M->compress(dealii::VectorOperation::add);
		iM->compress(dealii::VectorOperation::add);
	}
}


/// Local assemble on cell.
template<int dim>
void Assembler<dim>::local_assemble_cell(
	const typename dealii::DoFHandler<dim>::active_cell_iterator &cell,
	Assembly::Scratch::MeatAssembly<dim> &scratch,
	Assembly::CopyData::MeatAssembly<dim> &copydata) {
	
	// reinit scratch and data to current cell
	scratch.fe_values.reinit(cell);
	scratch.dofs_per_cell = scratch.fe_values.get_fe().dofs_per_cell;
	cell->get_dof_indices(copydata.local_dof_indices);
	
	// initialize local matrix with zeros
	copydata.ui_vi_matrix_block_L = 0;
	copydata.ui_vi_matrix_block_M = 0;
	copydata.ui_vi_matrix_block_iM = 0;
	
	// evaluate inverse conductivity tensor on all quadrature points
	iD->value_list(
		scratch.fe_values.get_quadrature_points(),
		scratch.iD_values
	);
	
	// assemble cell terms
	for (unsigned int q(0); q < scratch.fe_values.n_quadrature_points; ++q) {
		scratch.JxW = scratch.fe_values.JxW(q);
		
		// prefetch data
		for (unsigned int n(0); n < scratch.dofs_per_cell; ++n) {
			scratch.phi[n] =
				scratch.fe_values[flux].value(n,q);
			scratch.div_phi[n] =
				scratch.fe_values[flux].divergence(n,q);
			scratch.psi[n] =
				scratch.fe_values[pressure].value(n,q);
		}
		
		// loop over all trial & test function combinitions to get the assembly
		for (unsigned int i(0); i < scratch.dofs_per_cell; ++i) {
		for (unsigned int j(0); j < scratch.dofs_per_cell; ++j) {
			copydata.ui_vi_matrix_block_L(i,j) +=
				(scratch.iD_values[q] * scratch.phi[j] * scratch.phi[i]
				- scratch.div_phi[i] * scratch.psi[j]
				- scratch.div_phi[j] * scratch.psi[i]
				) * scratch.JxW;
			
			copydata.ui_vi_matrix_block_M(i,j) +=
				(scratch.psi[j] * scratch.psi[i]) * scratch.JxW;
		}} // for ij
	} // for q
	
	// compute local contribution of inverse mass matrix
	const unsigned int n_p = (unsigned int)std::pow(fe.tensor_degree(), dim);
	
	dealii::FullMatrix<double> local_mass;
	local_mass.reinit(n_p, n_p);
	const unsigned int m(copydata.ui_vi_matrix_block_M.m());
	for (unsigned int i(0); i < n_p; ++i)
	for (unsigned int j(0); j < n_p; ++j)
		local_mass[i][j] = copydata.ui_vi_matrix_block_M[i+m-n_p][j+m-n_p];
	
	local_mass.gauss_jordan();
	
	for (unsigned int i(0); i < n_p; ++i)
	for (unsigned int j(0); j < n_p; ++j)
		copydata.ui_vi_matrix_block_iM[i+m-n_p][j+m-n_p] = local_mass[i][j];
}


/// Copy local assembly to global matrix.
template<int dim>
void Assembler<dim>::copy_local_to_global_cell(
	const Assembly::CopyData::MeatAssembly<dim> &copydata) {
	constraints.distribute_local_to_global(
		copydata.ui_vi_matrix_block_L, copydata.local_dof_indices, *L
	);
	
	constraints.distribute_local_to_global(
		copydata.ui_vi_matrix_block_M, copydata.local_dof_indices, *M
	);
	
	constraints.distribute_local_to_global(
		copydata.ui_vi_matrix_block_iM, copydata.local_dof_indices, *iM
	);
}

}} // namespaces

#include "MEAT_Assembly.inst.in"
