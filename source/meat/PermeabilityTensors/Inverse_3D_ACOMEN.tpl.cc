/**
 * @file Inverse_3D_ACOMEN.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, cmake, UK
 * @date 2015-01-13, merge from MixedFE example 2014-01-30_DAAD for ACOMEN PAPER, UK
 * @date 2014-09-18, merged PermeabilityTensor from MixedFE example (DAAD 2014), UK
 * @date 2014-09-11, value function from MFEM (2013-06-12), UK
 * @date 2014-06-16, MEAT, UK
 * @date 2013-06-12, MFEM, UK
 * 
 * @brief Inverse_3D_ACOMEN for MFEM-Laplace.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#include <meat/PermeabilityTensors/Inverse_3D_ACOMEN.tpl.hh>

// MPI includes

// DEAL.II includes

// C++ includes
#include <vector>

namespace meat {
namespace PermeabilityTensors {

template<int rank, int dim>
Inverse_3D_ACOMEN<rank,dim>::
Inverse_3D_ACOMEN() {
	Assert(rank==2, dealii::ExcNotImplemented());
	AssertThrow(dim==3, dealii::ExcNotImplemented());
	
	std::vector<double> phi;
	std::vector<double> psi;
	std::vector<double> theta;
	
	std::vector<double> kx;
	std::vector<double> ky;
	std::vector<double> kz;
	
	phi.push_back(dealii::numbers::PI/9); // 20°
	psi.push_back(dealii::numbers::PI/18); // 10°
	theta.push_back(dealii::numbers::PI/36); // 5°
	
	phi.push_back(dealii::numbers::PI/2 - dealii::numbers::PI/9); // 70°
	psi.push_back(dealii::numbers::PI/18);
	theta.push_back(dealii::numbers::PI/36);
	
	kx.push_back(4000);
	kx.push_back(400);
	
	ky.push_back(2000);
	ky.push_back(200);
	
	kz.push_back(1000);
	kz.push_back(100);
	
	Qz.reinit(3,3);
	Qy.reinit(3,3);
	Qx.reinit(3,3);
	D.reinit(3,3);
	K.reinit(3,3);
	
	K1.reinit(3,3);
	K2.reinit(3,3);
	
	// setup K1
	Qz=0;
	Qz(0,0) = cos(phi[0]);
	Qz(0,1) = sin(phi[0]);
	Qz(1,0) = -sin(phi[0]);
	Qz(1,1) = cos(phi[0]);
	Qz(2,2) = 1.;
	
	Qy=0;
	Qy(0,0) = cos(psi[0]);
	Qy(0,2) = sin(psi[0]);
	Qy(1,1) = 1.;
	Qy(2,0) = -sin(psi[0]);
	Qy(2,2) = cos(psi[0]);
	
	Qx=0;
	Qx(0,0) = 1.;
	Qx(1,1) = cos(theta[0]);
	Qx(1,2) = sin(theta[0]);
	Qx(2,1) = -sin(theta[0]);
	Qx(2,2) = cos(theta[0]); 
	
	D = 0;
	D(0,0) = 1/kx[0];
	D(1,1) = 1/ky[0];
	D(2,2) = 1/kz[0];
	
	D.mmult(K,Qz);   // K  <- D * Qz
	K.mmult(D,Qy);   // D  <- K * Qy
	D.mmult(K,Qx);   // K  <- D * Qx
	Qz.Tmmult(D,K);  // D  <- Qz^T * K
	Qy.Tmmult(K,D);  // K  <- Qy^T * D
	Qx.Tmmult(K1,K); // K1 <- Qx^T * K
		
	// setup K2
	Qz=0;
	Qz(0,0) = cos(phi[1]);
	Qz(0,1) = sin(phi[1]);
	Qz(1,0) = -sin(phi[1]);
	Qz(1,1) = cos(phi[1]);
	Qz(2,2) = 1.;
	
	Qy=0;
	Qy(0,0) = cos(psi[1]);
	Qy(0,2) = sin(psi[1]);
	Qy(1,1) = 1.;
	Qy(2,0) = -sin(psi[1]);
	Qy(2,2) = cos(psi[1]);
	
	Qx=0;
	Qx(0,0) = 1.;
	Qx(1,1) = cos(theta[1]);
	Qx(1,2) = sin(theta[1]);
	Qx(2,1) = -sin(theta[1]);
	Qx(2,2) = cos(theta[1]); 
	
	D = 0;
	D(0,0) = 1/kx[1];
	D(1,1) = 1/ky[1];
	D(2,2) = 1/kz[1];
	
	D.mmult(K,Qz);   // K  <- D * Qz
	K.mmult(D,Qy);   // D  <- K * Qy
	D.mmult(K,Qx);   // K  <- D * Qx
	Qz.Tmmult(D,K);  // D  <- Qz^T * K
	Qy.Tmmult(K,D);  // K  <- Qy^T * D
	Qx.Tmmult(K2,K); // K2 <- Qx^T * K
	
// 	Q.print_formatted(cout);
// 	cout << endl;
// 	K1.print_formatted(cout);
// 	cout << endl;
// 	K2.print_formatted(cout);
// 	cout << endl;
}


template<int rank, int dim>
void
Inverse_3D_ACOMEN<rank,dim>::
value_list(
	const std::vector< dealii::Point<dim> > &points,
	std::vector< dealii::Tensor<rank,dim> > &values ) const {
	Assert(
		points.size() == values.size(),
		dealii::ExcDimensionMismatch(points.size(), values.size())
	);
	
	const dealii::FullMatrix<double> *K;
	
	for (unsigned int p(0); p < points.size(); ++p) {
		values[p].clear();
		
		if ( ((points[p][0] <= .3) && (points[p][1] <= .3)) ||
			 ((points[p][0] >= .3) && (points[p][1] >= .3) &&
			  (points[p][0] <= .7) && (points[p][1] <= .7)) ||
			 ((points[p][0] >= .7) && (points[p][1] >= .7)) ) {
			// K2 region
			K = &K2;
		}
		else
			K = &K1;
		
		for (unsigned int i(0); i < dim; ++i)
		for (unsigned int j(0); j < dim; ++j)
			values[p][i][j] = (*K)(i,j);
	}
}

}} //namespaces

#include "Inverse_3D_ACOMEN.inst.in"
