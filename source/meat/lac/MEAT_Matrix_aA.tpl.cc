/**
 * @file MEAT_Matrix_aA.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, UK
 * @date 2014-06-18, UK
 * @date 2014-06-17, UK
 * @date 2013-06-15, UK
 *
 * @brief Applies ~A*x = (B^T diag(M_q)^{-1} B)*x .
 *
 * This class implements the matrix-vector products vmult and Tvmult as
 * an application of an Approximated Schur complement stiffness matrix ~A
 * derived from the operator L.
 * This is useful as preconditioner.
 * The operator matrix L is assumed to have the following block structure:
 * L = [M_q B; B^T 0].
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */


// DTM++ includes
#include <meat/lac/MEAT_Matrix_aA.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/trilinos_block_sparse_matrix.h>
#include <deal.II/lac/trilinos_precondition.h>

// C++ includes
#include <memory>

namespace meat {
namespace lac {

template<typename VectorType>
MEAT_Matrix_aA<VectorType>::
MEAT_Matrix_aA(
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L,
	const VectorType &_q) :
	L(L), x1(_q), x2(_q) {
	// Initialise i_diag_M_q := diag(M_q)^{-1}.
	// The vmult operation is equivalent to the application of a vmult from a
	// Jacobi preconditioner with omega = 1.
	i_diag_M_q = std::make_shared< dealii::TrilinosWrappers::PreconditionJacobi >();
	dealii::TrilinosWrappers::PreconditionJacobi::AdditionalData additional_data;
	additional_data.min_diagonal = 0.;
	additional_data.omega = 1.;
	i_diag_M_q->initialize(L->block(0,0), additional_data);
	
	// Check if everything was initialised
	Assert(this->L.use_count(), dealii::ExcNotInitialized());
	Assert(this->i_diag_M_q.use_count(), dealii::ExcNotInitialized());
	Assert(this->x1.size(), dealii::ExcNotInitialized());
	Assert(this->x2.size(), dealii::ExcNotInitialized());
}


template<typename VectorType>
void
MEAT_Matrix_aA<VectorType>::vmult(VectorType &y, const VectorType &x0) const {
	L->block(0,1).vmult(x1,x0); // applies B
	i_diag_M_q->vmult(x2,x1);   // applies diag(M_q)^{-1}
	L->block(1,0).vmult(y,x2);  // applies B^T
}

}}

#include "MEAT_Matrix_aA.inst.in"
