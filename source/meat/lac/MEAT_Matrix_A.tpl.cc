/**
 * @file MEAT_Matrix_A.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, UK
 * @date 2014-06-18, UK
 * @date 2014-06-17, UK
 * @date 2013-06-15, UK
 *
 * @brief Applies A*x = (B^T M_q^{-1} B)*x .
 *
 * This class implements the matrix-vector products vmult and Tvmult as
 * an application of a Schur complement stiffness matrix A
 * derived from the operator L.
 * The operator matrix L is assumed to have the following block structure:
 * L = [M_q B; B^T 0].
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */


// DTM++ includes
#include <meat/lac/MEAT_Matrix_A.tpl.hh>

// DEAL.II includes

// C++ includes

namespace meat {
namespace lac {

template<typename VectorType>
MEAT_Matrix_A<VectorType>::
MEAT_Matrix_A(
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L,
	std::shared_ptr< DTM::core::lac::MatrixBase<VectorType> > iMq,
	const VectorType &_q) :
	L(L), iMq(iMq), x1(_q), x2(_q) {
	// Check if everything was initialised
	Assert(this->L.use_count(), dealii::ExcNotInitialized());
	Assert(this->iMq.use_count(), dealii::ExcNotInitialized());
	Assert(this->x1.size(), dealii::ExcNotInitialized());
	Assert(this->x2.size(), dealii::ExcNotInitialized());
}


template<typename VectorType>
void
MEAT_Matrix_A<VectorType>::vmult(VectorType &y, const VectorType &x0) const {
	L->block(0,1).vmult(x1,x0);
	iMq->vmult(x2,x1);
	L->block(1,0).vmult(y,x2);
}

}}

#include "MEAT_Matrix_A.inst.in"
