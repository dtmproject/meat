/**
 * @file MEAT_Matrix_iMq.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, UK
 * @date 2014-06-18, UK
 * @date 2014-06-17, UK
 * @date 2013-06-15, UK
 *
 * @brief Applies M_q^{-1}*x.
 *
 * This class implements the matrix-vector products vmult and Tvmult
 * derived from the operator L.
 * The operator matrix L is assumed to have the following block structure:
 * L = [M_q B; B^T 0].
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */


// DTM++ includes
#include <meat/lac/MEAT_Matrix_iMq.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/iterative_inverse.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/trilinos_block_sparse_matrix.h>
#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/trilinos_vector.h>

// C++ includes
#include <memory>

namespace meat {
namespace lac {

template<typename VectorType>
MEAT_Matrix_iMq<VectorType>::
MEAT_Matrix_iMq(
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L) :
	L(L) {
	// Check if everything was initialised
	Assert(this->L.use_count(), dealii::ExcNotInitialized());
	
	Mq = std::make_shared< dealii::TrilinosWrappers::SparseMatrix > ();
	Mq->copy_from(L->block(0,0));
	
	b = std::make_shared<VectorType>();
	y = std::make_shared<VectorType>();
	
	// create direct_solver
	direct_solver = std::make_shared< DTM::core::lss::SolverAmesos > ();
	factorised = false;
}

template<typename VectorType>
void
MEAT_Matrix_iMq<VectorType>::vmult(VectorType &_y, const VectorType &_x) const {
	if (!factorised) {
		b->reinit(_x);
		y->reinit(_y);
		direct_solver->factorise(Mq,y,b);
		factorised = true;
	}
	
	*b = _x;
	direct_solver->solve();
	
	_y = *y;
}

}} // namespaces

#include "MEAT_Matrix_iMq.inst.in"
