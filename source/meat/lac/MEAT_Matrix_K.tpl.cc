/**
 * @file MEAT_Matrix_K.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, UK
 * @date 2014-06-18, UK
 * @date 2014-06-17, UK
 * @date 2013-06-15, origin MFEM class, UK
 *
 * @brief Applies K * x = (mu M + tau_n/2 A)*x .
 *
 * This class implements the matrix-vector products vmult and Tvmult as
 *   K*x = = (mu M + tau_n/2 A)*x .
 * It take a reference on the mass matrix M and
 * on the operator A, e.g. A = B^T M_q^{-1} B implemented in MEAT_Matrix_A.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

// DTM++ includes
#include <meat/lac/MEAT_Matrix_K.tpl.hh>

// DEAL.II includes

// C++ includes

namespace meat {
namespace lac {

template<typename VectorType>
MEAT_Matrix_K<VectorType>::
MEAT_Matrix_K(
	std::shared_ptr< dealii::TrilinosWrappers::SparseMatrix > M,
	std::shared_ptr< DTM::core::lac::MatrixBase<VectorType> > A,
	const double mu,
	const double tau,
	const VectorType &vec_p) :
	M(M), A(A),
	mu(mu),
	tau(tau),
	y1(vec_p) {
	// Check if everything was initialised
	Assert(this->M.use_count(), dealii::ExcNotInitialized());
	Assert(this->A.use_count(), dealii::ExcNotInitialized());
	Assert(this->tau > 0, dealii::ExcNotInitialized());
	Assert(this->y1.size(), dealii::ExcNotInitialized());
}


template<typename VectorType>
void
MEAT_Matrix_K<VectorType>::vmult(VectorType &y, const VectorType &x) const {
	M->vmult(y, x);
	A->vmult(y1, x);
	y.sadd(mu, tau/2., y1);
}

}}

#include "MEAT_Matrix_K.inst.in"
