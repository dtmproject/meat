/**
 * @file dirichlet_ACOMEN.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, cmake, UK
 * @date 2015-01-13, merge from MixedFE example 2014-01-30_DAAD for ACOMEN PAPER, UK
 * @date 2014-09-18, merge from DAAD 3D example, UK
 * @date 2014-06-16, UK
 * @date 2013-10-18, UK
 *
 * @brief Dirichlet boundary function.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

// PROJECT includes
#include <meat/BoundaryValues/dirichlet_3D_ACOMEN.tpl.hh>

// MPI includes

// DEAL.II includes

// C++ includes
#include <limits>
#include <vector>
#include <string>

namespace meat {
namespace BoundaryValues {

template<int dim>
dirichlet_ACOMEN<dim>::
dirichlet_ACOMEN(std::string &parameters) : initialised(false) {
	initialise(parameters);
}

template<int dim>
void
dirichlet_ACOMEN<dim>::
initialise(std::string &parameters) {
	////////////////////////////////////////////////////////////////////////////
	// parse the input string, arguments are splitted with spaces
	//
	std::string argument;
	std::vector< std::string > arguments;
	for (auto &character : parameters) {
		if (!std::isspace(character)) {
			if ( character != '"' ) {
				argument += character;
			}
		}
		else {
			if (argument.size()) {
				arguments.push_back(argument);
				argument.clear();
			}
		}
	}
	
	if (argument.size()) {
		arguments.push_back(argument);
		argument.clear();
	}
	////////////////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////////////////////
	// convert the individual argument strings to double (needs C++.11)
	//
	alpha = std::stod(arguments.at(0));
	beta = std::stod(arguments.at(1));
	
	////////////////////////////////////////////////////////////////////////////
	initialised = true;
}



template<int dim>
double
dirichlet_ACOMEN<dim>::
value(
	const dealii::Point<dim> &p,
	const unsigned int
) const {
	AssertThrow(dim==3, dealii::ExcNotImplemented());
	Assert(initialised, dealii::ExcNotInitialized());
	
	double t = this->get_time();
	const double f(10);
	double result(0);
	
	switch (dim) {
		case 3:
			result += alpha * (1-p[0])*(p[1])*(p[2]); // top left corner (0,1,1)
			result += beta  * (p[0])  *(1-p[1])*(1-p[2]); // bottom right corner (1 0 0)
			
			return result * std::min(1., t);
		
		default:
			Assert(false, dealii::ExcNotImplemented());
	}
	
	return std::numeric_limits<double>::quiet_NaN();
}

}} // namespaces

#include "dirichlet_3D_ACOMEN.inst.in"
