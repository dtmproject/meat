/**
 * @file   ParameterSet.tpl.cc
 * @author Uwe Koecher (UK)
 * @date   2015-09-09, initial implementation, UK
 * 
 * @brief Keeps all parsed input parameters in a struct.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

// PROJECT includes
#include <meat/Parameters/ParameterSet.tpl.hh>

// DEAL.II includes

// C++ includes

namespace meat {
namespace Parameters {

ParameterSet::
ParameterSet(
	std::shared_ptr< dealii::ParameterHandler > handler) {
	
	dim = static_cast<unsigned int> (handler->get_integer("dim"));
	
	handler->enter_subsection("Problem Specification"); {
		p = static_cast<unsigned int> (handler->get_integer("p"));
		r = static_cast<unsigned int> (handler->get_integer("r"));
		
		inverse_permeability_tensor =
			handler->get("inverse permeability tensor");
		
		dirichlet_boundary = handler->get("Dirichlet boundary");
		dirichlet_boundary_parameters =
			handler->get("Dirichlet boundary parameters");
		
		force = handler->get("force");
		u0 = handler->get("u0");
	}
	handler->leave_subsection();
	
	handler->enter_subsection("Time Integration"); {
		t0 = handler->get_double("initial time");
		T = handler->get_double("final time");
		tau_n = handler->get_double("time step size");
	}
	handler->leave_subsection();
	
	handler->enter_subsection("Mesh Specification"); {
		use_mesh_input_file = handler->get_bool("use mesh input file");
		mesh_input_filename = handler->get("mesh input filename");
		
		GridGenerator = handler->get("GridGenerator");
		a = handler->get_double("a");
		b = handler->get_double("b");
	}
	handler->leave_subsection();
	
	handler->enter_subsection("Linear System Solver"); {
		outer_solver_tolerance = handler->get_double("tolerance");
		outer_solver_max_iter  = static_cast<unsigned int> (
			handler->get_integer("maximum iterations")
		);
		compute_all_condition_numbers = handler->get_bool(
			"compute all condition numbers"
		);
	}
	handler->leave_subsection();
	
	handler->enter_subsection("Output Quantities"); {
		data_output_trigger = handler->get_double("data output trigger time");
		
		if (handler->get_bool("data output patches auto mode")) {
			data_output_patches = p+1;
		}
		else {
			data_output_patches = static_cast<unsigned int> (
				handler->get_integer("data output patches")
			);
		}
	}
	handler->leave_subsection();
}

}} // namespace
