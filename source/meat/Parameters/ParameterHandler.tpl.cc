/**
 * @file   ParameterHandler.tpl.cc
 * @author Uwe Koecher (UK)
 * @date   2015-09-08, DTM++/meat, UK
 * @date   2015-02-27, UK
 * @date   2015-02-17, (ParametersSolver) UK
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

// PROJECT includes
#include <meat/Parameters/ParameterHandler.tpl.hh>

// DEAL.II includes
#include <deal.II/base/parameter_handler.h>

// C++ includes

namespace meat {
namespace Parameters {

ParameterHandler::
ParameterHandler() {
	declare_entry(
		"dim",
		"2",
		dealii::Patterns::Integer(),
		"dim"
	);
	
	enter_subsection("Problem Specification"); {
		declare_entry(
			"p",
			"0",
			dealii::Patterns::Integer(),
			"Polynomial degree p"
		);
		
		declare_entry(
			"r",
			"0",
			dealii::Patterns::Integer(),
			"Global refinements of the intial mesh"
		);
		
		declare_entry(
			"inverse permeability tensor",
			"Inverse_anyD_Identity",
			dealii::Patterns::Anything()
		);
		
		declare_entry(
			"Dirichlet boundary",
			"ZeroFunction",
			dealii::Patterns::Anything()
		);
		
		declare_entry(
			"Dirichlet boundary parameters",
			"ZeroFunction",
			dealii::Patterns::Anything()
		);
		
		declare_entry(
			"force",
			"ZeroFunction",
			dealii::Patterns::Anything()
		);
		
		declare_entry(
			"u0",
			"ZeroFunction",
			dealii::Patterns::Anything()
		);
	}
	leave_subsection();
	
	enter_subsection("Mesh Specification"); {
		declare_entry(
			"use mesh input file",
			"false",
			dealii::Patterns::Bool(),
			"determines whether to use an input file or a deal.II GridGenerator"
		);
		
		declare_entry(
			"mesh input filename",
			"./input/.empty",
			dealii::Patterns::Anything(),
			"filename of the mesh which can be read in with dealii::GridIn"
		);
		
		declare_entry(
			"GridGenerator",
			"hyper_cube",
			dealii::Patterns::Anything()
		);
		
		declare_entry(
			"a",
			"0.",
			dealii::Patterns::Double(),
			"GridGenerator::hyper_cube: [a,b]^dim"
		);
		
		declare_entry(
			"b",
			"1.",
			dealii::Patterns::Double(),
			"GridGenerator::hyper_cube: [a,b]^dim"
		);
	}
	leave_subsection();
	
	enter_subsection("Time Integration"); {
		declare_entry(
			"initial time",
			"0.",
			dealii::Patterns::Double(),
			"initial time t0"
		);
		
		declare_entry(
			"final time",
			"0.",
			dealii::Patterns::Double(),
			"final time T"
		);
		
		declare_entry(
			"time step size",
			"1e-2",
			dealii::Patterns::Double(),
			"initial time step size"
		);
	}
	leave_subsection();
	
	enter_subsection("Linear System Solver"); {
		declare_entry(
			"tolerance",
			"1e-10",
			dealii::Patterns::Double(),
			"outer solver tolerance"
		);
		
		declare_entry(
			"maximum iterations",
			"1000",
			dealii::Patterns::Double(),
			"maximum solver iterations til the failure criterion is met"
		);
		
		declare_entry(
			"compute all condition numbers",
			"false",
			dealii::Patterns::Bool(),
			"compute all condition numbers"
		);
	}
	leave_subsection();
	
	enter_subsection("Output Quantities");
		declare_entry(
			"data output trigger time",
			"0.",
			dealii::Patterns::Double(),
			"data output trigger time"
		);
		
		declare_entry(
			"data output patches auto mode",
			"true",
			dealii::Patterns::Bool(),
			"data output patches auto mode => using p+1 data output patches"
		);
		
		declare_entry(
			"data output patches",
			"1",
			dealii::Patterns::Integer(),
			"data output patches"
		);
	leave_subsection();
}

}} // namespace
