/**
 * @file Grid.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-09-03, cmake, UK
 * @date 2014-06-18, MEAT and C++11, UK
 * @date 2014-06-16, MEAT, UK
 * @date 2013-06-12, MFEM, UK
 *
 * @brief Handle a parallel distributed mesh. (needs p4est) MFEM
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#include <meat/Grid/Grid.tpl.hh>

// class declaration
namespace meat {
namespace Grid {

//member function definition

/// Constructor.
template<int dim>
Grid<dim>::
Grid(
	std::shared_ptr< dealii::FESystem<dim> > fe,
	std::shared_ptr< dealii::Mapping<dim> > mapping,
	MPI_Comm mpi_comm) :
	mpi_comm(mpi_comm),
	tria(std::make_shared< dealii::parallel::distributed::Triangulation<dim> >(
		mpi_comm,
		typename dealii::Triangulation<dim>::MeshSmoothing(
			dealii::Triangulation<dim>::smoothing_on_refinement |
			dealii::Triangulation<dim>::smoothing_on_coarsening
		)
	)),
	dof(std::make_shared< dealii::DoFHandler<dim> >(*tria)),
	fe(fe),
	mapping(mapping) {
}


/// Destructor. Clears DoFHandler.
template<int dim>
Grid<dim>::
~Grid() {
	if (dof.use_count()) {
		dof->clear();
	}
}


/// Generate grid. Throws Exception in base class.
template<int dim>
void
Grid<dim>::
generate() {
	AssertThrow(false, dealii::ExcNotImplemented());
}


/// Global refinement.
template<int dim>
void
Grid<dim>::
refine_global(const unsigned int n) {
	AssertThrow(tria.use_count(), dealii::ExcNotInitialized());
	tria->refine_global(n);
}


/// Distribute.
template<int dim>
void
Grid<dim>::
distribute() {
	// Distribute the degrees of freedom (dofs)
	AssertThrow(dof.use_count(), dealii::ExcNotInitialized());
	dof->distribute_dofs(*fe);
	dealii::DoFRenumbering::component_wise(*dof);
	
	// create partitioning for the initalisation of vectors
	std::vector< dealii::types::global_dof_index > dofs_per_component(
		dealii::DoFTools::n_components(*dof), 0
	);
	dealii::DoFTools::count_dofs_per_component(*dof, dofs_per_component);
	n_q = dofs_per_component[0]; // dofs of the flux field
	n_u = dofs_per_component[dim]; // dofs of the primal field
	
	locally_owned_dofs = std::make_shared< dealii::IndexSet >();
	dealii::DoFTools::extract_locally_owned_dofs(*dof, *locally_owned_dofs);
	
	locally_relevant_dofs = std::make_shared< dealii::IndexSet >();
	dealii::DoFTools::extract_locally_relevant_dofs(*dof, *locally_relevant_dofs);
	
	partitioning_locally_owned_dofs = std::make_shared< std::vector< dealii::IndexSet > >();
	partitioning_locally_owned_dofs->push_back(locally_owned_dofs->get_view(0,n_q));
	partitioning_locally_owned_dofs->push_back(locally_owned_dofs->get_view(n_q,n_q+n_u));
	
	partitioning_locally_relevant_dofs = std::make_shared< std::vector< dealii::IndexSet > >();
	partitioning_locally_relevant_dofs->push_back(locally_relevant_dofs->get_view(0,n_q));
	partitioning_locally_relevant_dofs->push_back(locally_relevant_dofs->get_view(n_q,n_q+n_u));
	
	// setup constraints object,
	// needed for distriubtion of the local assemblies in parallel
	constraints = std::make_shared< dealii::ConstraintMatrix > ();
	constraints->clear();
	constraints->reinit(*locally_relevant_dofs);
	constraints->close();
	
	// Now we can create a fitting sparsity pattern for our
	// TrilinosWrappers::BlockSparseMatrix
	dealii::Table<2,dealii::DoFTools::Coupling> coupling_block_M(dim+1,dim+1);
	dealii::Table<2,dealii::DoFTools::Coupling> coupling_block_L(dim+1,dim+1);
	
	for (unsigned int i(0); i < dim+1; ++i)
	for (unsigned int j(0); j < dim+1; ++j) {
		coupling_block_M[i][j] = ( (i==dim) && (j==dim) ) ?
			dealii::DoFTools::always : dealii::DoFTools::none;
		
		coupling_block_L[i][j] = ( (i==dim) && (j==dim) ) ?
			dealii::DoFTools::none : dealii::DoFTools::always;
	}
	
	sp_block_M = std::make_shared< dealii::TrilinosWrappers::BlockSparsityPattern > ();
	sp_block_M->reinit(*partitioning_locally_owned_dofs, mpi_comm);
	
	sp_block_L = std::make_shared< dealii::TrilinosWrappers::BlockSparsityPattern > ();
	sp_block_L->reinit(*partitioning_locally_owned_dofs, mpi_comm);
	
	dealii::DoFTools::make_sparsity_pattern(
		*dof, coupling_block_M, *sp_block_M, *constraints, false,
		dealii::Utilities::MPI::this_mpi_process(mpi_comm)
	);
	
	dealii::DoFTools::make_sparsity_pattern(
		*dof, coupling_block_L, *sp_block_L, *constraints, false,
		dealii::Utilities::MPI::this_mpi_process(mpi_comm)
	);
	
	sp_block_M->compress();
	sp_block_L->compress();
}

}} // namespaces

#include "Grid.inst.in"
