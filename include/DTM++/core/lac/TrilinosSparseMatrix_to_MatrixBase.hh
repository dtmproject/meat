/**
 * @file TrilinosSparseMatrix_to_MatrixBase.hh
 * @author Uwe Koecher (UK)
 * @date 2015-05-22, UK
 *
 * @brief TrilinosSparseMatrix_to_MatrixBase header file
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __TrilinosSparseMatrix_to_MatrixBase_hh
#define __TrilinosSparseMatrix_to_MatrixBase_hh

// DTM++ includes
#include <DTM++/core/lac/MatrixBase.hh>


// PROJECT includes

// MPI includes

// DEAL.II includes
#include <deal.II/base/subscriptor.h>
#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/trilinos_vector.h>

// TRILINOS includes

// C++ includes
#include <memory>

namespace DTM {
namespace core {
namespace lac {

template<class VectorType>
class TrilinosSparseMatrix_to_MatrixBase : public MatrixBase<VectorType> {
public:
	TrilinosSparseMatrix_to_MatrixBase(
		std::shared_ptr< dealii::TrilinosWrappers::SparseMatrix > A
	);
	
	virtual ~TrilinosSparseMatrix_to_MatrixBase() = default;
	
	virtual void vmult(VectorType &dst, const VectorType &src) const;
	virtual void Tvmult(VectorType &dst, const VectorType &src) const;
	virtual void vmult_add(VectorType &dst, const VectorType &src) const;
	virtual void Tvmult_add(VectorType &dst, const VectorType &src) const;
	
	std::shared_ptr< dealii::TrilinosWrappers::SparseMatrix > A;
};

}}} // namespaces

#endif
