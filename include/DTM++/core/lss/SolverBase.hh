/**
 * @file SolverBase.hh
 * @author Uwe Koecher (UK)
 * @date 2015-02-02, Rebase to DTM++.core library, UK
 * @date 2015-01-23, meat application, UK
 * @date 2014-06-14, UK
 *
 * @brief SolverBase header file
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __SOLVERBASE_HH
#define __SOLVERBASE_HH

// PROJECT includes

// MPI includes

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/solver_control.h>

// TRILINOS includes

// C++ includes
#include <memory>

namespace DTM {
namespace core {

namespace lac {
// forward declarations
class OperatorBase;
class VectorBase;

} // lac

namespace lss {
// forward declarations
class PreconditionBase;


class SolverBase {
public:
	/// Constructor.
	
	//! The Constructor takes the deal.II SolverControl object and creates
	//! the solver.
	SolverBase(
		std::shared_ptr<dealii::SolverControl> solver_control
	);
	
	/// Destructor.
	virtual ~SolverBase();
	
	/// Solve a linear system <tt>Ax = b</tt>.
	virtual void solve(
		const DTM::core::lac::OperatorBase &A,
		DTM::core::lac::VectorBase &x,
		const DTM::core::lac::VectorBase &b
	);
	
protected:
	std::shared_ptr<dealii::SolverControl> solver_control;
};


}}} // namespaces

#endif
