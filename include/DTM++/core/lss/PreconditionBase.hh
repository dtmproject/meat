/**
 * @file PreconditionBase.hh
 * @author Uwe Koecher (UK)
 * @date 2015-02-02, Rebase to DTM++.core library, UK
 * @date 2015-01-23, meat application, UK
 * @date 2014-06-14, UK
 *
 * @brief PreconditionBase header file
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __PRECONDITIONBASE_HH
#define __PRECONDITIONBASE_HH

// PROJECT includes

// MPI includes

// DEAL.II includes
#include <deal.II/base/subscriptor.h>

// TRILINOS includes

// C++ includes

namespace DTM {
namespace core {
namespace lss {

class PreconditionBase : public dealii::Subscriptor {
public:
	/// Constructor.
	PreconditionBase();
	
	/// Destructor.
	virtual ~PreconditionBase();
};


}}} // namespaces

#endif
