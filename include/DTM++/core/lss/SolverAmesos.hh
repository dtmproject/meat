/**
 * @file SolverAmesos.hh
 * @author Uwe Koecher (UK)
 * @date 2015-02-02, Rebase to DTM++.core library, UK
 * @date 2015-01-23, meat application, UK
 * @date 2014-09-12, UK
 * @date 2014-06-14, UK
 *
 * @brief SolverAmesos header file
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __SOLVERAMESOS_HH
#define __SOLVERAMESOS_HH

// PROJECT includes

// MPI includes

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/trilinos_vector.h>

// TRILINOS includes
#include <Teuchos_ParameterList.hpp>
#include <Epetra_LinearProblem.h>
#include <Amesos.h>
#include <Epetra_Operator.h>
#include <Amesos.h>
#include <Amesos_BaseSolver.h>


// C++ includes
#include <memory>

namespace DTM {
namespace core {
namespace lss {


class SolverAmesos {
public:
	/// Constructor.
	
	//! The Constructor takes the deal.II SolverControl object and creates
	//! the solver.
	SolverAmesos(
		std::shared_ptr<dealii::SolverControl> solver_control = nullptr,
		std::shared_ptr<Teuchos::ParameterList> solver_parameters = nullptr
	);
	
	/// Destructor.
	virtual ~SolverAmesos() = default;
	
	/// Factorise the linear system <tt>Ax = b</tt>.
	virtual void factorise(
		std::shared_ptr<const dealii::TrilinosWrappers::SparseMatrix> A,
		std::shared_ptr<dealii::TrilinosWrappers::MPI::Vector> x,
		std::shared_ptr<const dealii::TrilinosWrappers::MPI::Vector> b
	);
	
	/// Solve the linear system <tt>Ax = b</tt>.
	virtual void solve();
	
protected:
	std::shared_ptr<Amesos_BaseSolver> solver;
	std::shared_ptr<dealii::SolverControl> solver_control;
	
	std::shared_ptr<Epetra_LinearProblem> linear_problem;
	
	std::string SolverType;
};


}}} // namespaces

#endif
