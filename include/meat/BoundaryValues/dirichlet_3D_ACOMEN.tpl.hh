/**
 * @file dirichlet_ACOMEN.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, cmake, UK
 * @date 2015-01-13, merge from MixedFE example 2014-01-30_DAAD for ACOMEN PAPER, UK
 * @date 2014-09-18, merge from DAAD 3D example, UK
 * @date 2014-06-16, UK
 * @date 2013-10-18, UK
 *
 * @brief Dirichlet boundary function.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __dirichlet_ACOMEN_tpl_hh
#define __dirichlet_ACOMEN_tpl_hh

// PROJECT includes
#include <DTM++/core/base/Function.tpl.hh>

// MPI includes

// DEAL.II includes

// C++ includes
#include <string>

namespace meat {
namespace BoundaryValues {

template<int dim>
class dirichlet_ACOMEN : public DTM::core::Function<dim,1> {
public:
	dirichlet_ACOMEN(std::string &parameters);
	virtual ~dirichlet_ACOMEN() = default;
	
	virtual void initialise(std::string &parameters);
	
	virtual double value(
		const dealii::Point<dim> &x,
		const unsigned int component=0
	) const override final;
	
private:
	bool initialised;
	
	double alpha;
	double beta;
};


}} // namespace

#endif
