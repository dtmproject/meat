/**
 * @file MEAT_cG1.tpl.hh
 * @author Uwe Koecher
 * @date 2015-09-09, input parameters, UK
 * @date 2015-09-07, (cmake), UK
 * @date 2014-09-14 (updated to MEAT confirmity), UK
 * @date 2013-10-17 (initial release derived from stationary Laplace problem)
 *
 * @brief MEAT cG1 solver
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __MEAT_cG1_tpl_hh
#define __MEAT_cG1_tpl_hh

// PROJECT includes
#include <meat/BoundaryValues/all.hh>
#include <meat/PermeabilityTensors/all.hh>

#include <meat/Parameters/ParameterHandler.tpl.hh>
#include <meat/Parameters/ParameterSet.tpl.hh>

#include <meat/Grid/Grid.tpl.hh>
#include <meat/Grid/boundary_id.hh>
#include <meat/LocalIntegrators/MEAT_Assembly.tpl.hh>
#include <meat/LocalIntegrators/MEAT_Assembly_RHS.tpl.hh>

#include <meat/lac/MEAT_Matrix_iMq.tpl.hh>
#include <meat/lac/MEAT_Matrix_A.tpl.hh>
#include <meat/lac/MEAT_Matrix_aA.tpl.hh>
#include <meat/lac/MEAT_Matrix_K.tpl.hh>
#include <meat/lac/MEAT_Matrix_Inverse.tpl.hh>

// DTM++ includes
#include <DTM++/core/io/DataOutput.tpl.hh>

// DEAL.II includes
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/parameter_handler.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/base/utilities.h>

#include <deal.II/lac/linear_operator.h>

#include <deal.II/dofs/function_map.h>

#include <deal.II/grid/grid_generator.h>

#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_raviart_thomas.h>
#include <deal.II/fe/fe_dgq.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/lac/iterative_inverse.h>
#include <deal.II/lac/linear_operator.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_control.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/vector_tools.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <map>
#include <memory>

namespace meat {

template<int dim>
class MEAT_cG1 {
public:
	MEAT_cG1(
		std::ofstream &pout
	);
	
	virtual ~MEAT_cG1() = default;
	
	virtual void set_input_parameters(
		std::shared_ptr< dealii::ParameterHandler > parameter_handler
	);
	
	virtual void set_inverse_permeability_tensor(
		std::shared_ptr< dealii::TensorFunction<2,dim> > inverse_permeability
	);
	
	virtual void set_dirichlet_boundary(
		std::shared_ptr< dealii::Function<dim> > dirichlet_boundary
	);
	
	virtual void set_force(
		std::shared_ptr< dealii::Function<dim> > force
	);
	
	virtual void set_u0(
		std::shared_ptr< dealii::Function<dim> > u0
	);
	
	virtual void set_initial_tau(
		const double tau
	);
	
	virtual void set_data_output_trigger(double data_output_trigger);
	virtual void set_data_output_patches(unsigned int data_output_patches);
	
	virtual void create_function(
		std::string &function_string,
		std::shared_ptr< dealii::Function<dim> > &function
	);
	
	virtual void init_functions_from_parameters();
	
	virtual void init();
	
	virtual void run();
	
protected:
	virtual void assemble_system(); ///< assembles L = [A B; B^T 0], M, M^{-1}
	virtual void assemble_rhs(const double t1); ///< assemble and calculate rhs
	
	virtual void do_data_output(
		double &trigger_time_variable, ///< external output time
		const double t0,               ///< left endpoint of I_n
		const double tn,               ///< right endpoint of I_n
		const double trigger_increment
	);
	
	std::shared_ptr< dealii::FESystem<dim> > fe;
	std::shared_ptr< dealii::Mapping<dim> > mapping;
	
	std::shared_ptr< meat::Grid::Grid<dim> > grid;
	
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L; ///< Stiffness assembly block system matrix.
	std::shared_ptr< dealii::TrilinosWrappers::SparseMatrix > M;      ///< Mass assembly matrix.
	std::shared_ptr< dealii::TrilinosWrappers::SparseMatrix > iM;     ///< Inverse Mass assembly matrix.
	std::shared_ptr< meat::lac::MEAT_Matrix_iMq
		< dealii::TrilinosWrappers::MPI::Vector> > iMq; // M_q^{-1}
		
	std::shared_ptr< meat::lac::MEAT_Matrix_K
		<dealii::TrilinosWrappers::MPI::Vector>> A1; // A1 = (mu1 M + tau/2 A)
	
	dealii::TrilinosWrappers::MPI::BlockVector  x0,x1,xn; ///< Block-System solution x_i=(q_i,u_i).
	std::shared_ptr< dealii::TrilinosWrappers::MPI::BlockVector > x_trigger;
	
	dealii::TrilinosWrappers::MPI::BlockVector  bb1;     ///< Block-System right hand side.
	
	dealii::TrilinosWrappers::MPI::Vector vec_M_u0; ///< vec_M_u0 = M * u0
	
	dealii::TrilinosWrappers::MPI::Vector b; // Schur complement rhs vectors
	dealii::TrilinosWrappers::MPI::Vector qq; // helping vectors for creating b
	
	std::shared_ptr< dealii::Function<dim> > force;              ///< Force function.
	std::shared_ptr< dealii::Function<dim> > dirichlet_boundary; ///< Dirichlet function.
	
	std::shared_ptr< dealii::Function<dim> > initial_value_function_u0; ///< Initial value function.
	
	std::shared_ptr< dealii::TensorFunction<2,dim> > inverse_permeability; ///< Inverse permeability tensor function D^{-1)(x)

private:
	std::ofstream &pout; ///< process output stream
	unsigned int p;      ///< FE polynomial degree
	
	double tau;          /// time discretisation parameter
	
	std::unique_ptr< meat::Assemble::RHS::Assembler<dim> >
		assembler_rhs_1;
	
	DTM::core::MFEM::DataOutput<dim> data_output;
	
	double data_output_trigger;
	double data_output_time_value;
	unsigned int data_output_patches;
	
	dealii::ConditionalOStream pcout; // output to std::cout only be root process.
	
	std::shared_ptr< meat::Parameters::ParameterSet > parameter_set;
};

} // namespaces

#endif
