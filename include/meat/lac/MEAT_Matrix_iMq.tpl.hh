/**
 * @file MEAT_Matrix_iMq.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, UK
 * @date 2014-06-18, UK
 * @date 2014-06-17, UK
 * @date 2013-06-15, UK
 *
 * @brief Applies M_q^{-1}*x.
 *
 * This class implements the matrix-vector products vmult and Tvmult
 * derived from the operator L.
 * The operator matrix L is assumed to have the following block structure:
 * L = [M_q B; B^T 0].
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __MEAT_Matrix_iMq_tpl_hh
#define __MEAT_Matrix_iMq_tpl_hh

// DEFINES
#ifndef MAX_ITER
#define MAX_ITER 1000
#endif

#include <mpi.h>

// DTM++ includes
#include <DTM++/core/lac/MatrixBase.hh>
#include <DTM++/core/lss/SolverAmesos.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/iterative_inverse.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/trilinos_block_sparse_matrix.h>
#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/trilinos_vector.h>

// C++ includes
#include <memory>

namespace meat {
namespace lac {

template<class VectorType>
class MEAT_Matrix_iMq : public DTM::core::lac::MatrixBase<VectorType> {
public:
	MEAT_Matrix_iMq(
		std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L
	);
	
	virtual ~MEAT_Matrix_iMq() = default;
	
	virtual void vmult (VectorType &dst, const VectorType &src) const override final;
	virtual void Tvmult (VectorType &dst, const VectorType &src) const override final {
		vmult(dst,src);
	};
	
protected:
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L;
	std::shared_ptr< dealii::TrilinosWrappers::SparseMatrix > Mq;
	
	std::shared_ptr< DTM::core::lss::SolverAmesos > direct_solver;
	mutable std::shared_ptr<VectorType> b,y;
	mutable bool factorised;
};

}} //namespaces

#endif
