/**
 * @file MEAT_Matrix_Inverse.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-08, UK
 * @date 2014-08-08, origin from MEAT_Matrix_K and MEAT_Matrix_iMq, UK
 * @date 2014-06-18, UK
 * @date 2014-06-17, UK
 * @date 2013-06-15, origin MFEM class, UK
 *
 * @brief Applies iA * x = A^{-1}*x .
 *
 * This class implements the matrix-vector products vmult and Tvmult as
 *   A^{-1}*x .
 * It take a reference on a matrix A implemented as
 *   DTMpp::core::MatrixBase<VectorType>.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __MEAT_Matrix_Inverse_tpl_hh
#define __MEAT_Matrix_Inverse_tpl_hh

// DEFINES
#ifndef MAX_ITER
#define MAX_ITER 1000
#endif

// DTM++ includes
#include <DTM++/core/lac/MatrixBase.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/iterative_inverse.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/trilinos_sparse_matrix.h>

// C++ includes
#include <memory>

namespace meat {
namespace lac {

template<class VectorType>
class MEAT_Matrix_Inverse : public DTM::core::lac::MatrixBase<VectorType> {
public:
	MEAT_Matrix_Inverse(
		std::shared_ptr< DTM::core::lac::MatrixBase<VectorType> > A
	);
	
	virtual ~MEAT_Matrix_Inverse();
	
	virtual void vmult (VectorType &dst, const VectorType &src) const override final;
	virtual void Tvmult (VectorType &dst, const VectorType &src) const override final {
		vmult(dst,src);
	};
	
protected:
	std::shared_ptr< DTM::core::lac::MatrixBase<VectorType> > A;
	std::shared_ptr< dealii::IterativeInverse<VectorType> > iA;
	
	mutable std::shared_ptr< dealii::PreconditionIdentity > inner_precondition;
};

}}

#endif
