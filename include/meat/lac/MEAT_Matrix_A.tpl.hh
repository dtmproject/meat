/**
 * @file MEAT_Matrix_A.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, UK
 * @date 2014-06-18, UK
 * @date 2014-06-17, UK
 * @date 2013-06-15, UK
 *
 * @brief Applies A*x = (B^T M_q^{-1} B)*x .
 *
 * This class implements the matrix-vector products vmult and Tvmult as
 * an application of a Schur complement stiffness matrix A
 * derived from the operator L.
 * The operator matrix L is assumed to have the following block structure:
 * L = [M_q B; B^T 0].
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __MEAT_Matrix_A_tpl_hh
#define __MEAT_Matrix_A_tpl_hh

// DTM++ includes
#include <DTM++/core/lac/MatrixBase.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/trilinos_block_sparse_matrix.h>

// C++ includes
#include <memory>

namespace meat {
namespace lac {

template<class VectorType>
class MEAT_Matrix_A : public DTM::core::lac::MatrixBase< VectorType > {
public:
	MEAT_Matrix_A(
		std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L,
		std::shared_ptr< DTM::core::lac::MatrixBase<VectorType> > iMq,
		const VectorType &vec_q
	);
	
	virtual ~MEAT_Matrix_A() = default;
	
	virtual void vmult (VectorType &dst, const VectorType &src) const override final;
	virtual void Tvmult (VectorType &dst, const VectorType &src) const override final {
		vmult(dst,src);
	};
	
protected:
	std::shared_ptr<dealii::TrilinosWrappers::BlockSparseMatrix> L;
	std::shared_ptr<DTM::core::lac::MatrixBase<VectorType>> iMq;
	
	mutable VectorType x1,x2;
};

}}

#endif
