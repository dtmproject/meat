/**
 * @file MEAT_Matrix_K.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, UK
 * @date 2014-06-18, UK
 * @date 2014-06-17, UK
 * @date 2013-06-15, origin MFEM class, UK
 *
 * @brief Applies K * x = (mu M + tau_n/2 A)*x .
 *
 * This class implements the matrix-vector products vmult and Tvmult as
 *   K*x = = (mu M + tau_n/2 A)*x .
 * It take a reference on the mass matrix M and
 * on the operator A, e.g. A = B^T M_q^{-1} B implemented in MEAT_Matrix_A.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __MEAT_Matrix_K_tpl_hh
#define __MEAT_Matrix_K_tpl_hh

// DTM++ includes
#include <DTM++/core/lac/MatrixBase.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/lac/trilinos_sparse_matrix.h>

// C++ includes
#include <memory>

namespace meat {
namespace lac {

template<class VectorType>
class MEAT_Matrix_K : public DTM::core::lac::MatrixBase<VectorType> {
public:
	MEAT_Matrix_K(
		std::shared_ptr< dealii::TrilinosWrappers::SparseMatrix > M,
		std::shared_ptr< DTM::core::lac::MatrixBase<VectorType> > A,
		const double mu,
		const double tau,
		const VectorType &vec_p
	);
	
	virtual ~MEAT_Matrix_K() = default;
	
	virtual void vmult (VectorType &dst, const VectorType &src) const override final;
	virtual void Tvmult (VectorType &dst, const VectorType &src) const override final {
		vmult(dst,src);
	};
	
protected:
	std::shared_ptr< dealii::TrilinosWrappers::SparseMatrix > M;
	std::shared_ptr< DTM::core::lac::MatrixBase<VectorType> > A;
	
	const double mu;
	double tau;
	
	mutable VectorType y1;
};

}}

#endif
