/**
 * @file Inverse_3D_ACOMEN.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, cmake, UK
 * @date 2015-01-13, merge from MixedFE example 2014-01-30_DAAD for ACOMEN PAPER, UK
 * @date 2014-09-18, merged PermeabilityTensor from MixedFE example (DAAD 2014), UK
 * @date 2014-09-11, value function from MFEM (2013-06-12), UK
 * @date 2014-06-16, MEAT, UK
 * @date 2013-06-12, MFEM, UK
 * 
 * @brief Inverse_3D_ACOMEN for MFEM-Laplace.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __Inverse_3D_ACOMEN_tpl_hh
#define __Inverse_3D_ACOMEN_tpl_hh

#include <DTM++/core/base/TensorFunction.tpl.hh>

// MPI includes

// DEAL.II includes
#include <deal.II/lac/full_matrix.h>

// C++ includes
#include <vector>

namespace meat {
namespace PermeabilityTensors {

template<int rank, int dim>
class Inverse_3D_ACOMEN : public DTM::core::TensorFunction<rank,dim> {
public:
	Inverse_3D_ACOMEN();
	virtual ~Inverse_3D_ACOMEN() = default;
	
	virtual void value_list(
		const std::vector< dealii::Point<dim> > &points,
		std::vector< dealii::Tensor<rank,dim> > &values
	) const;
	
private:
	dealii::FullMatrix<double> K1,K2,K,D,Qz,Qy,Qx;
};

}}

#endif
