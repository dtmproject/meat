/**
 * @file   ParameterSet.tpl.hh
 * @author Uwe Koecher (UK)
 * @date   2015-09-09, initial implementation, UK
 * 
 * @brief Keeps all parsed input parameters in a struct.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __ParameterSet_tpl_hh
#define __ParameterSet_tpl_hh

// PROJECT includes

// DEAL.II includes
#include <deal.II/base/parameter_handler.h>

// C++ includes
#include <string>
#include <memory>

namespace meat {
namespace Parameters {

struct ParameterSet {
	ParameterSet( std::shared_ptr< dealii::ParameterHandler > handler );
	
	unsigned int dim;
	unsigned int p;
	unsigned int r;
	
	std::string inverse_permeability_tensor;
	
	std::string dirichlet_boundary;
	std::string dirichlet_boundary_parameters;
	
	std::string force;
	std::string u0;
	
	bool use_mesh_input_file;
	std::string mesh_input_filename;
	std::string GridGenerator;
	double a;
	double b;
	
	double t0;
	double T;
	double tau_n;
	
	double outer_solver_tolerance;
	unsigned int outer_solver_max_iter;
	bool compute_all_condition_numbers;
	
	double data_output_trigger;
	unsigned int data_output_patches;
};


}} // namespace

#endif
