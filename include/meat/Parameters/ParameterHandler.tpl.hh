/**
 * @file   ParameterHandler.tpl.hh
 * @author Uwe Koecher (UK)
 * @date   2015-09-08, DTM++/meat, UK
 * @date   2015-02-27, DTM++/CCFD, UK
 * @date   2015-02-17, (ParametersSolver) UK
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __ParameterHandler_tpl_hh
#define __ParameterHandler_tpl_hh

// PROJECT includes

// DEAL.II includes
#include <deal.II/base/parameter_handler.h>

// C++ includes

namespace meat {
namespace Parameters {

class ParameterHandler : public dealii::ParameterHandler {
public:
	ParameterHandler();
	virtual ~ParameterHandler() = default;
};


}} // namespace

#endif
