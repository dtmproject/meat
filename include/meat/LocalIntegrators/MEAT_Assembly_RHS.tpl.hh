/**
 * @file MEAT_Assembly_RHS.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-06, cmake, UK
 * @date 2014-06-16, MEAT, UK
 * @date 2013-10-17, MFEM, UK
 *
 * @brief Purpose: Assemble right hand side of mixed FEM Heat problem.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __MEAT_Assembly_RHS_tpl_hh
#define __MEAT_Assembly_RHS_tpl_hh

// PROJECT includes

// DEAL.II includes
#include <deal.II/fe/fe.h>
#include <deal.II/fe/mapping.h>
#include <deal.II/fe/fe_update_flags.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/lac/trilinos_block_vector.h>

#include <deal.II/grid/filtered_iterator.h>

#include <deal.II/base/exceptions.h>
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/work_stream.h>

// C++ includes
#include <functional>
#include <iterator>
#include <vector>
#include <memory>

namespace meat {
namespace Assemble {
namespace RHS {

namespace Assembly {
namespace Scratch {

/// Struct for scratch on local cell matrix.
template<int dim>
struct MeatAssemblyRHS {
	MeatAssemblyRHS(
		const dealii::FiniteElement<dim> &fe,
		const dealii::Mapping<dim> &mapping,
		const dealii::Quadrature<dim> &quad,
		const dealii::Quadrature<dim-1> &quad_face,
		const dealii::UpdateFlags &uflags_cell,
		const dealii::UpdateFlags &uflags_face
	);

	MeatAssemblyRHS(const MeatAssemblyRHS &scratch);

	dealii::FEValues<dim>                fe_values;
	dealii::FEFaceValues<dim>            fe_face_values;
	std::vector< dealii::Tensor<1,dim> > phi;
	std::vector<double>                  psi;
	
	std::vector<double>                  f_values;
	std::vector<double>                  g_values;
	std::vector< dealii::Point<dim> >    normal_vector;
	
	double                               JxW;
	unsigned int                         dofs_per_cell;
};

} // namespace Scratch
namespace CopyData {

/// Struct for copydata on local cell matrix.
template<int dim>
struct MeatAssemblyRHS {
	MeatAssemblyRHS(const dealii::FiniteElement<dim> &fe);
	MeatAssemblyRHS(const MeatAssemblyRHS &copydata);

	dealii::Vector<double> bi_vi_vector;
	std::vector<unsigned int> local_dof_indices;
};

} // namespace CopyData
} // namespace Assembly
////////////////////////////////////////////////////////////////////////////////


/** Assembler.
 *
 */
template<int dim>
class Assembler {
public:
	/** Assemble mixed FEM system right hand side of the Heat problem.
	 * Here the RHS will be assembled into the block-vector
	 * b := [(-g, phi_i*n)_{partial Omega}; (-f, psi_i) ], which has block structure.
	 * Please make sure, that you initialized the matrix L before with some
	 * fitting sparsity pattern and initialize it on yourself.
	 * The FiniteElement fe must be as FESystem( RT(p), DG_Q(p) ) and dof the
	 * fitting DoFHandler with renumbered DoFs (flux, pressure).
	 */
	Assembler(
		dealii::TrilinosWrappers::MPI::BlockVector &b,
		dealii::ConstraintMatrix &constraints,
		dealii::FiniteElement<dim> &fe,
		dealii::Mapping<dim> &mapping,
		dealii::DoFHandler<dim> &dof,
		dealii::UpdateFlags uflags_cell =
			dealii::update_values |
			dealii::update_quadrature_points |
			dealii::update_JxW_values,
		dealii::UpdateFlags uflags_face =
			dealii::update_values |
			dealii::update_normal_vectors |
			dealii::update_quadrature_points |
			dealii::update_JxW_values
	);
	
	~Assembler() = default;
	
	/** Assemble matrix. Matrix must be initialized before!
	 *  If @param n_quadrature_points = 0 is given,
	 *  the dynamic default fe.tensor_degree()+1 will be used.
	 */
	void assemble(
		std::shared_ptr< dealii::Function<dim> > force,
		std::shared_ptr< dealii::Function<dim> > dirichlet_boundary,
		const bool compress = true,
		const unsigned int n_quadrature_points = 0
	);
	
protected:
	void local_assemble_cell(
		const typename dealii::DoFHandler<dim>::active_cell_iterator &cell,
		Assembly::Scratch::MeatAssemblyRHS<dim> &scratch,
		Assembly::CopyData::MeatAssemblyRHS<dim> &copydata
	);
	
	void copy_local_to_global_cell(
		const Assembly::CopyData::MeatAssemblyRHS<dim> &copydata
	);
	
private:
	dealii::TrilinosWrappers::MPI::BlockVector &b;
	dealii::ConstraintMatrix &constraints;
	dealii::FiniteElement<dim> &fe;
	dealii::Mapping<dim> &mapping;
	dealii::DoFHandler<dim> &dof;

	dealii::UpdateFlags uflags_cell;
	dealii::UpdateFlags uflags_face;

	std::shared_ptr< dealii::Function<dim> > force;
	std::shared_ptr< dealii::Function<dim> > dirichlet_boundary;
	
	// FEValuesExtractors for determining FE of flux respectively pressure.
	const dealii::FEValuesExtractors::Vector flux;
	const dealii::FEValuesExtractors::Scalar pressure;
};

}}} // namespaces

#endif
