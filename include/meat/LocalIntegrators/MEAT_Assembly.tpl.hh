/**
 * @file MEAT_Assembly.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-04, cmake, UK
 * @date 2014-06-17, MEAT, UK
 * @date 2014-06-16, MEAT, UK
 * @date 2013-10-17, MFEM, UK
 *
 * @brief Purpose: Assemble MFEM_Heat_Assembly
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __MEAT_Assembly_tpl_hh
#define __MEAT_Assembly_tpl_hh

// PROJECT includes

// DEAL.II includes
#include <deal.II/fe/fe.h>
#include <deal.II/fe/mapping.h>
#include <deal.II/fe/fe_update_flags.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/lac/block_sparsity_pattern.h>
#include <deal.II/lac/trilinos_block_sparse_matrix.h>

#include <deal.II/grid/filtered_iterator.h>

#include <deal.II/base/exceptions.h>
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/base/work_stream.h>

#include <deal.II/dofs/dof_tools.h>

// C++ includes
#include <functional>
#include <iterator>
#include <memory>
#include <vector>

namespace meat {
namespace Assemble {

namespace Assembly {
namespace Scratch {

/// Struct for scratch on local cell matrix.
template<int dim>
struct MeatAssembly {
	MeatAssembly(
		const dealii::FiniteElement<dim> &fe,
		const dealii::Mapping<dim> &mapping,
		const dealii::Quadrature<dim> &quad,
		const dealii::UpdateFlags &uflags_cell
	);
	
	MeatAssembly(const MeatAssembly &scratch);

	dealii::FEValues<dim>               fe_values;
	std::vector< dealii::Tensor<1,dim> > phi;
	std::vector<double>         div_phi;
	std::vector<double>         psi;
	
	double                      JxW;
	unsigned int                dofs_per_cell;
	
	std::vector< dealii::Tensor<2,dim> > iD_values;
};

} // namespace Scratch
namespace CopyData {

/// Struct for copydata on local cell matrix.
template<int dim>
struct MeatAssembly {
	MeatAssembly(const dealii::FiniteElement<dim> &fe);
	MeatAssembly(const MeatAssembly &copydata);

	dealii::FullMatrix<double> ui_vi_matrix_block_L;
	dealii::FullMatrix<double> ui_vi_matrix_block_M;
	dealii::FullMatrix<double> ui_vi_matrix_block_iM;
	std::vector<unsigned int> local_dof_indices;
};

} // namespace CopyData
} // namespace Assembly
////////////////////////////////////////////////////////////////////////////////


/** Assembler.
 *
 */
template<int dim>
class Assembler {
public:
	/** Assemble mixed weak heat operator.
	 * Here, the mixed weak heat operator will be assembled into the matricies
	 * L := [A B; B^T 0], M := [0 0; 0 M], iM := [0 0; 0 iM],
	 * which have block structure.
	 * Please make sure, that you initialized the matricies before with some
	 * fitting sparsity pattern and initialize it on yourself.
	 * The FiniteElement fe must be an FESystem( RT(p), DG_Q(p) ) and
	 * the belonging DoFHandler dof must have the renumbered DoFs (flux, pressure).
	 */
	Assembler(
		std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L,
		std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > M,
		std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > iM,
		dealii::ConstraintMatrix &constraints,
		dealii::FiniteElement<dim> &fe,
		dealii::Mapping<dim> &mapping,
		dealii::DoFHandler<dim> &dof,
		dealii::UpdateFlags uflags =
			dealii::update_values |
			dealii::update_gradients |
			dealii::update_quadrature_points |
			dealii::update_JxW_values
	);
	~Assembler() = default;
	
	/** Assemble matrix. Matrix must be initialized before!
	 *  If @param n_quadrature_points = 0 is given,
	 *  the dynamic default fe.tensor_degree()+1 will be used.
	 */
	void assemble(
		std::shared_ptr< dealii::TensorFunction<2,dim> > inverse_conductivity_tensor,
		const bool compress = true,
		const unsigned int n_quadrature_points = 0
	);
	
protected:
	void local_assemble_cell(
		const typename dealii::DoFHandler<dim>::active_cell_iterator &cell,
		Assembly::Scratch::MeatAssembly<dim> &scratch,
		Assembly::CopyData::MeatAssembly<dim> &copydata
	);
	
	void copy_local_to_global_cell(
		const Assembly::CopyData::MeatAssembly<dim> &copydata
	);
	
private:
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > L;
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > M;
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparseMatrix > iM;
	
	dealii::ConstraintMatrix &constraints;
	dealii::FiniteElement<dim> &fe;
	dealii::Mapping<dim> &mapping;
	dealii::DoFHandler<dim> &dof;

	dealii::UpdateFlags uflags;

	std::shared_ptr< dealii::TensorFunction<2,dim> > iD;
	
	// FEValuesExtractors for determining FE of flux respectively pressure.
	const dealii::FEValuesExtractors::Vector flux;
	const dealii::FEValuesExtractors::Scalar pressure;
};

}} // namespaces

#endif
