/**
 * @file Grid.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-09-03, cmake, UK
 * @date 2014-06-18, MEAT and C++11, UK
 * @date 2014-06-16, MEAT, UK
 * @date 2013-06-12, MFEM, UK
 *
 * @brief Handle a parallel distributed mesh. (needs p4est) MFEM
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/meat. (Diffusion Equation Solver)              */
/*                                                                            */
/*  DTM++/meat is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/meat is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/meat.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __Grid_tpl_hh
#define __Grid_tpl_hh

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/function.h>
#include <deal.II/base/index_set.h>
#include <deal.II/base/mpi.h>
#include <deal.II/base/utilities.h>

#include <deal.II/distributed/tria.h>
#include <deal.II/distributed/grid_refinement.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/mapping.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_in.h>
#include <deal.II/grid/grid_out.h>

#include <deal.II/lac/constraint_matrix.h>
#include <deal.II/lac/block_sparsity_pattern.h>

#include <deal.II/numerics/vector_tools.h>

// C++ includes
#include <memory>

#include <mpi.h>

// class declaration
namespace meat {
namespace Grid {

template<int dim>
class Grid {
public:
	Grid(
		std::shared_ptr< dealii::FESystem<dim> > fe,
		std::shared_ptr< dealii::Mapping<dim> > mapping,
		MPI_Comm mpi_comm = MPI_COMM_WORLD
	);
	
	virtual ~Grid();
	
	virtual void generate();
	
	virtual void refine_global(const unsigned int n = 1);
	
	virtual void set_boundary_indicators() { };
	
	virtual void distribute();
	
	MPI_Comm mpi_comm;
	std::shared_ptr< dealii::parallel::distributed::Triangulation<dim> > tria;
	std::shared_ptr< dealii::DoFHandler<dim> > dof;
	std::shared_ptr< dealii::FESystem<dim> > fe;
	std::shared_ptr< dealii::Mapping<dim> >  mapping;
	
	std::shared_ptr< dealii::IndexSet > locally_owned_dofs;
	std::shared_ptr< dealii::IndexSet > locally_relevant_dofs;
	std::shared_ptr< std::vector< dealii::IndexSet > > partitioning_locally_owned_dofs;
	std::shared_ptr< std::vector< dealii::IndexSet > > partitioning_locally_relevant_dofs;
	dealii::types::global_dof_index n_u, n_q;
	
	std::shared_ptr< dealii::ConstraintMatrix > constraints;
	
	// Block sparsity patterns for the lower order in time 2-by-2 block systems:
	//   primal variable mass block (also for the inverse mass),
	//   (lower right block)
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparsityPattern > sp_block_M;
	//  mixed blocks (upper left, upper right and lower left blocks)
	std::shared_ptr< dealii::TrilinosWrappers::BlockSparsityPattern > sp_block_L;
	
	dealii::GridIn<dim>        grid_in;
	dealii::GridOut            grid_out;
};

}} // namespaces

#endif
